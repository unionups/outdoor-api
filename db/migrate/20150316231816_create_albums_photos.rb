class CreateAlbumsPhotos < ActiveRecord::Migration
  def change
    create_table :albums_photos, id: false  do |t|
      t.belongs_to :photo, null: false
      t.belongs_to :album, null: false
    end

    add_index :albums_photos, :photo_id
    add_index :albums_photos, :album_id

  end
end
