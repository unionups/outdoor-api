class CreateRegions < ActiveRecord::Migration
  def change
    create_table :regions do |t|
      t.belongs_to :country
      t.string :name, null: false, unique: true
      t.integer :latitude
      t.integer :longitude
      t.text :description
      
      t.integer :publisher_id
      t.boolean :published, default: false
      t.datetime :published_at
      t.boolean :is_changed, default: true
      t.string  :changed_items, default: "all"
      t.boolean :is_backup, default: false
      
      t.timestamps
    end
  end
end
