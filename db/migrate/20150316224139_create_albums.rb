class CreateAlbums < ActiveRecord::Migration
  def change
    create_table :albums do |t|
      t.belongs_to :gallery

      t.string :name, default: "main"
      t.text :description
      t.timestamps
    end
  end
end
