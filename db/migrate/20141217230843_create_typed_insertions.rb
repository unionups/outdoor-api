class CreateTypedInsertions < ActiveRecord::Migration
  def change
    create_table :typed_insertions do |t|
      # t.belongs_to :location
      # t.string :location_type
      t.references :location, polymorphic: true, index: true

      t.string :type
      t.string :insertion_type
      t.string :name, null: false
      t.text :description

      t.integer :publisher_id
      t.boolean :is_published, default: false
      t.boolean :is_backup, default: false
      t.timestamps
    end
  end
end
