class CreateCountries < ActiveRecord::Migration
  def change
    create_table :countries do |t|
      t.string :name, null: false, unique: true
      t.string :cca2, null: false, unique: true
      t.string :cca3, null: false, unique: true
      t.integer :latitude
      t.integer :longitude
      t.text :description
      t.text :visa_info
      t.string :government_page
      t.text :transport_info

      t.integer :clone_of_id 
      t.integer :publisher_id
      t.boolean :published, default: false
      t.datetime :published_at
      t.boolean :is_changed, default: true
      t.string  :changed_items, default: "all"
      t.boolean :is_backup, default: false

      t.timestamps
    end
  end
end
