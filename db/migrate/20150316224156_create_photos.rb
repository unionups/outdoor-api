class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.string :name
      t.text :description

      t.integer :publisher_id
      t.boolean :published, default: false
      t.datetime :published_at

      t.timestamps
    end
  end
end
