# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150316232059) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "albums", force: true do |t|
    t.integer  "gallery_id"
    t.string   "name",        default: "main"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "albums_photos", id: false, force: true do |t|
    t.integer "photo_id", null: false
    t.integer "album_id", null: false
  end

  add_index "albums_photos", ["album_id"], name: "index_albums_photos_on_album_id", using: :btree
  add_index "albums_photos", ["photo_id"], name: "index_albums_photos_on_photo_id", using: :btree

  create_table "countries", force: true do |t|
    t.string   "name",                            null: false
    t.string   "cca2",                            null: false
    t.string   "cca3",                            null: false
    t.integer  "latitude"
    t.integer  "longitude"
    t.text     "description"
    t.text     "visa_info"
    t.string   "government_page"
    t.text     "transport_info"
    t.integer  "clone_of_id"
    t.integer  "publisher_id"
    t.boolean  "published",       default: false
    t.datetime "published_at"
    t.boolean  "is_changed",      default: true
    t.string   "changed_items",   default: "all"
    t.boolean  "is_backup",       default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "galleries", force: true do |t|
    t.integer  "location_id"
    t.string   "location_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "galleries", ["location_id", "location_type"], name: "index_galleries_on_location_id_and_location_type", using: :btree

  create_table "photos", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "publisher_id"
    t.boolean  "published",          default: false
    t.datetime "published_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "regions", force: true do |t|
    t.integer  "country_id"
    t.string   "name",                          null: false
    t.integer  "latitude"
    t.integer  "longitude"
    t.text     "description"
    t.integer  "publisher_id"
    t.boolean  "published",     default: false
    t.datetime "published_at"
    t.boolean  "is_changed",    default: true
    t.string   "changed_items", default: "all"
    t.boolean  "is_backup",     default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "subregions", force: true do |t|
    t.integer  "region_id"
    t.string   "name",                          null: false
    t.integer  "latitude"
    t.integer  "longitude"
    t.text     "description"
    t.integer  "publisher_id"
    t.boolean  "published",     default: false
    t.datetime "published_at"
    t.boolean  "is_changed",    default: true
    t.string   "changed_items", default: "all"
    t.boolean  "is_backup",     default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "typed_insertions", force: true do |t|
    t.integer  "location_id"
    t.string   "location_type"
    t.string   "type"
    t.string   "insertion_type"
    t.string   "name",                           null: false
    t.text     "description"
    t.integer  "publisher_id"
    t.boolean  "is_published",   default: false
    t.boolean  "is_backup",      default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "typed_insertions", ["location_id", "location_type"], name: "index_typed_insertions_on_location_id_and_location_type", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",     null: false
    t.string   "encrypted_password",     default: "",     null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,      null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "authentication_token"
    t.string   "role",                   default: "user"
    t.string   "lang",                   default: "en"
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
