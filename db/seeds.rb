# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.destroy_all
Country.destroy_all

users = User.create([
  {
   email: 'unionups@gmail.com',
   password: '12345678', password_confirmation: '12345678', role: "user"
  },
  {
   email: 'moder@gmail.com',
   password: '12345678', password_confirmation: '12345678', role: "moderator", lang: "ru"
  },
  {
   email: 'admin@gmail.com',
   password: '12345678', password_confirmation: '12345678', role: "admin"
  }

]) 



country = Country.create(
    name: "Ukraine",
    cca2: "UA",
    cca3: "UKR",
    description: "Some Description",
    visa_info: "Some Visa Info",
    government_page: "http://www.kmu.gov.ua/",
    transport_info: "Some Transpor Info",
    latitude: 49, 
    longitude: 32, 
    
    

    publisher_id: users.first.id,
    published: true
  )

  country.transport_hubs.create([
    {insertion_type: "airport", name: "BORYSPIL", description: "international airport"}, 
    {insertion_type: "airport", name: "KYIV", description: "international airport"}
    ])
  country.contacts.create([
    {insertion_type: "phone", name: "+38(097)23-20-789", description: "airport..."}, 
    {insertion_type: "phone", name: "+38(097)23-20-789", description: "airport..."}
    ])

  region = country.regions.create name: "Vinnitsa",
   description: "Regional center....", 
   latitude: 48, 
   longitude: 93,
   publisher_id: users.first.id, published: true
  region.transport_hubs.create([
    {insertion_type: "airport", name: "BORYSPIL", description: "international airport"}, 
    {insertion_type: "airport", name: "KYIV", description: "international airport"}
    ])
  region.contacts.create([
    {insertion_type: "phone", name: "+38(097)23-20-789", description: "airport..."}, 
    {insertion_type: "phone", name: "+38(097)23-20-789", description: "airport..."}
    ])

  region = country.regions.create name: "Lviv", 
    description: "Regional center....", 
    latitude: 48, 
    longitude: 93,
    publisher_id: users.first.id, published: true
  region.transport_hubs.create([
    {insertion_type: "airport", name: "BORYSPIL", description: "international airport"}, 
    {insertion_type: "airport", name: "KYIV", description: "international airport"}
    ])
  region.contacts.create([
    {insertion_type: "phone", name: "+38(097)23-20-789", description: "airport..."}, 
    {insertion_type: "phone", name: "+38(097)23-20-789", description: "airport..."}
    ])

  subregion = region.subregions.create name: "Ladyzhin", description: "Beauty town....", publisher_id: users.first.id, published: true
  subregion.transport_hubs.create([
    {insertion_type: "airport", name: "BORYSPIL", description: "international airport"}, 
    {insertion_type: "airport", name: "KYIV", description: "international airport"}
    ])
  subregion.contacts.create([
    {insertion_type: "phone", name: "+38(097)23-20-789", description: "airport..."}, 
    {insertion_type: "phone", name: "+38(097)23-20-789", description: "airport..."}
    ])
  subregion.accommodations.create([
    {insertion_type: "hotel", name: "Relax", description: "Hotel..."}, 
    {insertion_type: "wildcamping", name: "Big Foot", description: "Wildcamping..."}
    ])


  Country.create(
    name: "Slovakia",
    cca2: "SK",
    cca3: "SVK",
    description: "Some Description",
    visa_info: "Some Visa Info",
    government_page: "http://www.kmu.gov.ua/",
    transport_info: "Some Transpor Info",
    latitude: 48.66666666, 
    longitude: 19.5, 
    publisher_id: users.first.id
)

