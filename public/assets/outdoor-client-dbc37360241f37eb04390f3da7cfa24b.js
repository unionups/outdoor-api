/* jshint ignore:start */

/* jshint ignore:end */

define('outdoor-client/adapters/application', ['exports', 'ember-data'], function (exports, DS) {

	'use strict';

	var ApplicationAdapter;

	ApplicationAdapter = DS['default'].ActiveModelAdapter.extend({});

	exports['default'] = ApplicationAdapter;

});
define('outdoor-client/adapters/photo', ['exports', 'outdoor-client/adapters/application', 'ember-cli-form-data/mixins/form-data-adapter'], function (exports, ApplicationAdapter, FormDataAdapterMixin) {

	'use strict';

	var PhotoAdapter;

	PhotoAdapter = ApplicationAdapter['default'].extend(FormDataAdapterMixin['default'], {});

	exports['default'] = PhotoAdapter;

});
define('outdoor-client/app', ['exports', 'ember', 'ember/resolver', 'ember/load-initializers', 'outdoor-client/config/environment'], function (exports, Ember, Resolver, loadInitializers, config) {

  'use strict';

  var App;

  Ember['default'].MODEL_FACTORY_INJECTIONS = true;

  App = Ember['default'].Application.extend({
    modulePrefix: config['default'].modulePrefix,
    podModulePrefix: config['default'].podModulePrefix,
    Resolver: Resolver['default']
  });

  loadInitializers['default'](App, config['default'].modulePrefix);

  exports['default'] = App;

});
define('outdoor-client/components/editable-property', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  var EditablePropertyComponent;

  EditablePropertyComponent = Ember['default'].Component.extend({
    classNames: ["panel", "editable-property-panel"],
    classNameBindings: ["class", "isHower:callout"],
    isHower: false,
    isEdit: false,
    "class": null,
    property: null,
    propertyTemp: null,
    action: "propertyChanged",
    mouseEnter: function() {
      return this.set("isHower", true);
    },
    mouseLeave: function() {
      if (!this.get("isEdit")) {
        return this.set("isHower", false);
      }
    },
    actions: {
      onEdit: function() {
        this.set("propertyTemp", this.get("property"));
        return this.set("isEdit", true);
      },
      endEdit: function() {
        var propertyTemp;
        this.set("isHower", false);
        propertyTemp = this.get("propertyTemp");
        if (this.get("property") !== propertyTemp) {
          this.set("property", propertyTemp);
          this.sendAction('action');
        }
        return this.set("isEdit", false);
      }
    }
  });

  exports['default'] = EditablePropertyComponent;

});
define('outdoor-client/components/em-checkbox', ['exports', 'ember', 'ember-idx-forms/checkbox'], function (exports, Ember, CheckboxComponent) {

	'use strict';

	exports['default'] = CheckboxComponent['default'];

});
define('outdoor-client/components/em-file', ['exports', 'ember', 'ember-idx-forms/input'], function (exports, Ember, InputComponent) {

  'use strict';

  var EmFileComponent;

  EmFileComponent = InputComponent['default'].extend({
    type: "file",
    input: null,
    file: null,
    dataURL: null,
    didInsertElement: function() {
      var input;
      input = Foundation.utils.S(".em-file-field-wrapper input");
      input.change((function(_this) {
        return function(event) {
          var file, fileReader, files;
          files = event.target.files;
          file = files[0];
          _this.set("file", file);
          fileReader = new FileReader();
          fileReader.addEventListener("load", function(event) {
            var dataURL;
            dataURL = event.target.result;
            return _this.set("dataURL", dataURL);
          });
          return fileReader.readAsDataURL(file);
        };
      })(this));
      return this.set("input", input);
    },
    actions: {
      addFile: function() {
        this.get("input").click();
        return false;
      }
    }
  });

  exports['default'] = EmFileComponent;

});
define('outdoor-client/components/em-form-control-help', ['exports', 'ember', 'ember-idx-forms/control_help'], function (exports, Ember, FormControlHelperComponent) {

	'use strict';

	exports['default'] = FormControlHelperComponent['default'];

});
define('outdoor-client/components/em-form-group', ['exports', 'ember', 'ember-idx-forms/group'], function (exports, Ember, FormGroupComponent) {

	'use strict';

	exports['default'] = FormGroupComponent['default'];

});
define('outdoor-client/components/em-form-label', ['exports', 'ember', 'ember-idx-forms/label'], function (exports, Ember, FormLabelComponent) {

	'use strict';

	exports['default'] = FormLabelComponent['default'];

});
define('outdoor-client/components/em-form-submit', ['exports', 'ember', 'ember-idx-forms/submit_button'], function (exports, Ember, SubmitButtonComponent) {

	'use strict';

	exports['default'] = SubmitButtonComponent['default'];

});
define('outdoor-client/components/em-form', ['exports', 'ember', 'ember-idx-forms/form'], function (exports, Ember, FormComponent) {

	'use strict';

	exports['default'] = FormComponent['default'];

});
define('outdoor-client/components/em-gallery', ['exports', 'ember', 'ember-masonry-grid/masonry'], function (exports, Ember, MasonryGrid) {

  'use strict';

  var EmGalleryComponent;

  EmGalleryComponent = MasonryGrid['default'].extend({
    layoutName: "em-gallery",
    elements: Ember['default'].A(),
    fullElement: null,
    fullImageClasses: "full",
    randomWidth: (function() {
      var sid, thumbSizes;
      thumbSizes = [10, 20, 40];
      sid = (Math.random() * 3) << 0;
      return "width: " + thumbSizes[sid] + "%";
    }).property().volatile(),
    itemsChanged: (function() {
      return Ember['default'].run.once(this, 'itemsPrepended');
    }).observes('items.length'),
    itemsPrepended: function() {
      var addedItem;
      addedItem = this.get("items.lastObject");
      addedItem.set("style", "opacity: 0;");
      this.get("elements").unshiftObject(addedItem);
      return this.$().imagesLoaded((function(_this) {
        return function() {
          addedItem.set("style", "opacity: 1;");
          return _this.initializeMasonry();
        };
      })(this));
    },
    actions: {
      showFull: function(element) {
        var fic, fullImageClasses;
        fullImageClasses = this.get("fullImageClasses");
        fic = fullImageClasses.split(' ');
        fic.removeObject("show");
        this.set("fullImageClasses", fic.join(" "));
        this.set("fullElement", element);
        fic.push("show");
        return this.set("fullImageClasses", fic.join(" "));
      },
      toPrev: function() {
        var ci, elements, fullEl;
        elements = this.get("elements");
        fullEl = this.get("fullElement");
        ci = elements.indexOf(fullEl);
        if (ci === 0) {
          ci = elements.length;
        }
        return this.set("fullElement", elements.objectAt(ci - 1));
      },
      toNext: function() {
        var ci, elements, fullEl;
        elements = this.get("elements");
        fullEl = this.get("fullElement");
        ci = elements.indexOf(fullEl) + 1;
        if (elements.length === ci) {
          ci = 0;
        }
        return this.set("fullElement", elements.objectAt(ci));
      },
      close: function() {
        return this.set("fullElement", null);
      }
    }
  });

  exports['default'] = EmGalleryComponent;

});
define('outdoor-client/components/em-input', ['exports', 'ember', 'ember-idx-forms/input'], function (exports, Ember, InputComponent) {

	'use strict';

	exports['default'] = InputComponent['default'];

});
define('outdoor-client/components/em-select', ['exports', 'ember', 'ember-idx-forms/select'], function (exports, Ember, SelectComponent) {

	'use strict';

	exports['default'] = SelectComponent['default'];

});
define('outdoor-client/components/em-text', ['exports', 'ember', 'ember-idx-forms/text'], function (exports, Ember, TextComponent) {

	'use strict';

	exports['default'] = TextComponent['default'];

});
define('outdoor-client/components/gm-location-autocomplite', ['exports', 'ember', 'outdoor-client/components/simple-select', 'outdoor-client/mixins/in-gmap'], function (exports, Ember, SimpleSelectComponent, InGmapMixin) {

  'use strict';

  var GmLocationAutocompliteComponent;

  GmLocationAutocompliteComponent = SimpleSelectComponent['default'].extend(InGmapMixin['default'], {
    init: function() {
      this._super();
      return this.set("autocompliteService", new google.maps.places.AutocompleteService());
    },
    content: null,
    searchInputElement: null,
    predictions: null,
    mapBinding: "gmapComponent.map",
    latitudeBinding: "gmapComponent.latitude",
    longitudeBinding: "gmapComponent.longitude",
    infoWindowBinding: "gmapComponent.infoWindow",
    markerBinding: "gmapComponent.marker",
    updateMap: function() {
      return this.get("gmapComponent").send("cleanMap");
    },
    didInsertElement: function() {
      this.set("placesService", new google.maps.places.PlacesService(this.get("map")));
      return this.updateMap();
    },
    mapObserver: (function() {
      return this.updateMap();
    }).observes("map"),
    choosen: (function() {
      var searchInputElement, selectElement;
      selectElement = this.get("selectElement");
      $(selectElement).chosen();
      searchInputElement = $("div.chosen-search").children();
      this.set("searchInputElement", searchInputElement);
      searchInputElement.on("input propertychange", Ember['default'].run.bind(this, this.searchInputChange));
      return $(selectElement).on("DOMSubtreeModified", Ember['default'].run.bind(this, this.selectDidChange));
    }).observes("selectElement"),
    searchInputChange: function() {
      var autocompliteService, opts, predictionsPromise;
      autocompliteService = this.get("autocompliteService");
      if ((autocompliteService != null) && (this.get("searchInputElement")[1].value != null)) {
        opts = {
          input: this.get("searchInputElement")[1].value,
          types: ['(regions)'],
          componentRestrictions: {
            country: 'ua'
          }
        };
        predictionsPromise = new Ember['default'].RSVP.Promise(function(resolve, reject) {
          return autocompliteService.getQueryPredictions(opts, function(predictions, status) {
            if (status !== google.maps.places.PlacesServiceStatus.OK) {
              return reject(new Error("Cannot load prediction"));
            } else {
              return resolve(predictions);
            }
          });
        });
        return predictionsPromise.then((function(_this) {
          return function(predictions) {
            predictions = predictions.map(function(prediction) {
              return {
                name: prediction.terms[0].value,
                place_id: prediction.place_id
              };
            });
            _this.set("predictions", predictions);
            predictions = predictions.map(function(prediction) {
              return prediction.name;
            });
            return _this.set("content", predictions);
          };
        })(this));
      }
    },
    selectDidChange: function() {
      var selectElement;
      selectElement = this.get("selectElement");
      return $(selectElement).trigger("chosen:updated");
    },
    changeObserver: (function() {
      var map, placesService, prediction, val;
      val = this.get("value");
      placesService = this.get("placesService");
      map = this.get("map");
      prediction = this.get("predictions").findBy("name", val);
      if (prediction.place_id != null) {
        return placesService.getDetails({
          placeId: prediction.place_id
        }, (function(_this) {
          return function(place, status) {
            var infoWindow, latitude, longitude, marker;
            if (status === google.maps.places.PlacesServiceStatus.OK) {
              latitude = place.geometry.location.lat();
              longitude = place.geometry.location.lng();
              _this.updateMap();
              _this.set("location.name", prediction.name);
              _this.set("latitude", latitude);
              _this.set("location.latitude", latitude);
              _this.set("longitude", longitude);
              _this.set("location.longitude", longitude);
              marker = _this.get("marker");
              infoWindow = _this.get("infoWindow");
              marker.setPosition(place.geometry.location);
              marker.setIcon({
                url: "https://maps.gstatic.com/mapfiles/ms2/micons/POI.png",
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(35, 35)
              });
              marker.setVisible(true);
              infoWindow.setContent('<div><strong><h5>' + prediction.name + '</h5></strong></div>');
              return infoWindow.open(map, marker);
            }
          };
        })(this));
      }
    }).observes("value")
  });

  exports['default'] = GmLocationAutocompliteComponent;

});
define('outdoor-client/components/gm-map', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  var GmMapComponent;

  GmMapComponent = Ember['default'].Component.extend({
    mapCanvas: Ember['default'].View.extend({
      classNames: ["map-canvas"],
      didInsertElement: function() {
        return this.set("parentView.mapElement", this.get("element"));
      }
    }),
    mapElement: null,
    insertMap: (function() {
      var map, mapElement, mapOptions;
      mapElement = this.get("mapElement");
      if (mapElement != null) {
        mapOptions = {
          scrollwheel: false,
          center: new google.maps.LatLng(this.get("latitude"), this.get("longitude")),
          zoom: this.get("zoom"),
          mapTypeId: this.get("mapTypeId")
        };
        map = new google.maps.Map(mapElement, mapOptions);
        this.set("infoWindow", new google.maps.InfoWindow());
        this.set("marker", new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        }));
        return this.set("map", map);
      }
    }).observes("mapElement"),
    centerMap: (function() {
      var map;
      map = this.get("map");
      if (map != null) {
        return map.setCenter(new google.maps.LatLng(this.get("latitude"), this.get("longitude")));
      }
    }).observes("latitude", "longitude"),
    control: null,
    latitude: 0,
    longitude: 0,
    zoom: 5,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    map: null,
    geoJsonData: null,
    infoWindow: null,
    marker: null,
    actions: {
      cleanMap: function() {
        var geoData, geoJsonData, infoWindow, map, marker, _i, _len;
        map = this.get("map");
        if (map != null) {
          geoJsonData = this.get("geoJsonData");
          infoWindow = this.get("infoWindow");
          marker = this.get("marker");
          if (geoJsonData != null) {
            for (_i = 0, _len = geoJsonData.length; _i < _len; _i++) {
              geoData = geoJsonData[_i];
              map.data.remove(geoData);
            }
          }
          infoWindow.close();
          return marker.setVisible(false);
        }
      }
    }
  });

  exports['default'] = GmMapComponent;

});
define('outdoor-client/components/gm-select', ['exports', 'ember', 'outdoor-client/components/simple-select', 'outdoor-client/mixins/in-gmap'], function (exports, Ember, SimpleSelectComponent, InGmapMixin) {

  'use strict';

  var GmSelectComponent;

  GmSelectComponent = SimpleSelectComponent['default'].extend(InGmapMixin['default'], {
    content: null,
    searchInputElement: null,
    map: Ember['default'].computed.alias("gmapComponent.map"),
    geoJsonData: Ember['default'].computed.alias("gmapComponent.geoJsonData"),
    latitude: Ember['default'].computed.alias("gmapComponent.latitude"),
    longitude: Ember['default'].computed.alias("gmapComponent.longitude"),
    infoWindow: Ember['default'].computed.alias("gmapComponent.infoWindow"),
    marker: Ember['default'].computed.alias("gmapComponent.marker"),
    updateMap: function() {
      var defaultSelection;
      if (this.get("map") != null) {
        defaultSelection = this.get("defaultSelection");
        if (defaultSelection == null) {
          return this.get("gmapComponent").send("cleanMap");
        } else {
          return this.set("value", defaultSelection.get("id"));
        }
      }
    },
    didInsertElement: function() {
      return this.updateMap();
    },
    mapObserver: (function() {
      return this.updateMap();
    }).observes("map"),
    changeObserver: (function() {
      var cca2, cca3, infoWindow, map, marker, name, obj, val;
      val = this.get("value");
      if (val != null) {
        map = this.get("map");
        if (map != null) {
          this.get("gmapComponent").send("cleanMap");
          if ($.isNumeric(val)) {
            this.sendAction("action", val);
            obj = this.get("content").findBy("id", val);
            if (obj != null) {
              this.set("latitude", obj.get("latitude"));
              this.set("longitude", obj.get("longitude"));
              name = obj.get("name");
              if (obj.get("cca2") != null) {
                cca2 = obj.get("cca2");
                cca3 = obj.get("cca3").toLowerCase();
              }
            }
          } else {
            obj = this.get("content").findBy("name", val);
            if (obj != null) {
              this.sendAction("action", obj);
              this.set("latitude", obj.latitude);
              this.set("longitude", obj.longitude);
              name = obj.name;
              if (obj.cca2 != null) {
                cca2 = obj.cca2;
                cca3 = obj.cca3.toLowerCase();
              }
            }
          }
          infoWindow = this.get("infoWindow");
          marker = this.get("marker");
          marker.setIcon({
            url: "https://maps.gstatic.com/mapfiles/ms2/micons/POI.png",
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
          });
          marker.setPosition(new google.maps.LatLng(this.get("latitude"), this.get("longitude")));
          marker.setVisible(true);
          if (cca2 != null) {
            Ember['default'].$.getJSON("countries-geojson/" + cca3 + ".geo.json", (function(_this) {
              return function(geoJson) {
                return _this.set("geoJsonData", map.data.addGeoJson(geoJson));
              };
            })(this));
            infoWindow.setContent('<div><strong><h5>' + name + '</h5></strong>' + cca2 + '<br><img src="countries-geojson/' + cca3 + '.svg" style="width: 70px"></div>');
          } else {
            infoWindow.setContent('<div><strong><h5>' + name + '</h5></strong></div>');
          }
          return infoWindow.open(map, marker);
        }
      }
    }).observes("value"),
    choosen: (function() {
      var selectElement;
      selectElement = this.get("selectElement");
      return $(selectElement).chosen();
    }).observes("selectElement")
  });

  exports['default'] = GmSelectComponent;

});
define('outdoor-client/components/loading-slider', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  exports['default'] = Ember['default'].Component.extend({
    tagName: 'div',
    classNames: ['loading-slider'],
    classNameBindings: 'expanding',
    manage: function() {
      if (this.get('isLoading')) {
        if (this.get('expanding')) {
          this.expandingAnimate.call(this);
        } else {
          this.animate.call(this);
        }
      } else {
        this.set('isLoaded', true);
      }
    }.observes('isLoading'),
    animate: function() {
      this.set('isLoaded', false);
      var self = this,
          elapsedTime = 0,
          inner = $('<span>'),
          outer = this.$(),
          duration = this.getWithDefault('duration', 300),
          innerWidth = 0,
          outerWidth = this.$().width(),
          stepWidth = Math.round(outerWidth / 50),
          color = this.get('color');

      outer.append(inner);
      if (color) {
        inner.css('background-color', color);
      }

      var interval = window.setInterval(function() {
        elapsedTime = elapsedTime + 10;
        inner.width(innerWidth = innerWidth + stepWidth);

        // slow the animation if we used more than 75% the estimated duration
        // or 66% of the animation width
        if (elapsedTime > (duration * 0.75) || innerWidth > (outerWidth * 0.66)) {
          // don't stop the animation completely
          if (stepWidth > 1) {
            stepWidth = stepWidth * 0.97;
          }
        }

        if (innerWidth > outerWidth) {
          Ember['default'].run.later(function() {
            outer.empty();
            window.clearInterval(interval);
          }, 50);
        }

        // the activity has finished
        if (self.get('isLoaded')) {
          // start with a sizable pixel step
          if (stepWidth < 10) {
            stepWidth = 10;
          }
          // accelerate to completion
          stepWidth = stepWidth + stepWidth;
        }
      }, 10);
    },
    expandingAnimate: function() {
      var self = this,
          outer = this.$(),
          speed = this.getWithDefault('speed', 1000),
          colorQueue = this.get('color');

      if ('object' === typeof colorQueue) {
        var speedInterval = window.setInterval(function() {
          var color = colorQueue.shift();
          colorQueue.push(color);
          self.expandItem.call(self, color);
          if ( ! self.get('isLoading')) {
            window.clearInterval(speedInterval);
            outer.empty();
          }
        }, speed);
      } else {
        this.expandItem.call(this, colorQueue, true);
      }
    },
    expandItem: function(color, cleanUp) {
      var self = this,
          inner = $('<span>').css({
            'background-color': color,
          }),
          outer = this.$(),
          innerWidth = 0,
          outerWidth = outer.width(),
          stepWidth = Math.round(outerWidth / 50);

      outer.append(inner);

      var interval = window.setInterval(function() {
        var step = (innerWidth = innerWidth + stepWidth);
        if (innerWidth > outerWidth) {
          window.clearInterval(interval);
          if (cleanUp) {
            outer.empty();
          }
        }
        inner.css({
          'margin-left': '-' + step / 2 + 'px',
          'width': step,
        });
      }, 10);
    },
    didInsertElement: function() {
      this.$().html('<span>');

      var color = this.get('color');
      if (color) {
        this.$('span').css('background-color', color);
      }
    }
  });

});
define('outdoor-client/components/masonry-grid', ['exports', 'ember', 'ember-masonry-grid/masonry'], function (exports, Ember, MasonryComponent) {

	'use strict';

	exports['default'] = MasonryComponent['default'];

});
define('outdoor-client/components/reveal-modal', ['exports', 'ember', 'outdoor-client/mixins/foundation'], function (exports, Ember, FoundationMixin) {

  'use strict';

  var RevealModalComponent;

  RevealModalComponent = Ember['default'].Component.extend(FoundationMixin['default'], {
    layoutName: "reveal-modal",
    classNames: ["reveal-modal"],
    classNameBindings: ["size"],
    size: void 0,
    attributeBindings: ["id", "dataReveal:data-reveal"],
    dataReveal: "data-reveal",
    id: void 0,
    open: false,
    changeState: function() {
      var open;
      open = this.get("open");
      if (open) {
        return this.$().foundation('reveal', 'open');
      } else {
        return this.$().foundation('reveal', 'close');
      }
    },
    openObserver: (function() {
      return this.changeState();
    }).observes("open"),
    didInsertElement: function() {
      this._super();
      return Ember['default'].run.scheduleOnce('afterRender', this, this.afterRenderElement);
    },
    afterRenderElement: function() {
      var _this;
      _this = this;
      this.changeState();
      return this.$(document).on('close.fndtn.reveal', '[data-reveal]', function() {
        return _this.send("close");
      });
    },
    actions: {
      close: function() {
        return this.sendAction("close");
      }
    }
  });

  exports['default'] = RevealModalComponent;

});
define('outdoor-client/components/simple-select', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  var SimpleSelectComponent;

  SimpleSelectComponent = Ember['default'].Component.extend({
    action: "selectChanged",
    selectElement: null,
    selectView: Ember['default'].Select.extend({
      attributeBindings: ["chosenPlaceholder:data-placeholder"],
      chosenPlaceholderBinding: "parentView.prompt",
      valueBinding: "parentView.value",
      contentBinding: 'parentView.content',
      optionLabelPathBinding: 'parentView.optionLabelPath',
      optionValuePathBinding: 'parentView.optionValuePath',
      promptBinding: 'parentView.prompt',
      selectionBinding: "parentView.selection",
      didInsertElement: function() {
        return this.set("parentView.selectElement", this.get("element"));
      }
    }),
    optionValuePath: 'content',
    optionLabelPath: 'content',
    selection: (function() {
      return this.get("defaultSelection");
    }).property("defaultSelection"),
    changeObserver: (function() {
      var val;
      val = this.get("value");
      if (val) {
        return this.sendAction("action", val);
      }
    }).observes("value")
  });

  exports['default'] = SimpleSelectComponent;

});
define('outdoor-client/controllers/application', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  var ApplicationController;

  ApplicationController = Ember['default'].Controller.extend({
    isOpenModal: false,
    modalContent: "",
    lang: "en"
  });

  exports['default'] = ApplicationController;

});
define('outdoor-client/controllers/login', ['exports', 'ember', 'simple-auth/mixins/login-controller-mixin', 'ember-validations'], function (exports, Ember, LoginControllerMixin, EmberValidations) {

  'use strict';

  var LoginController;

  LoginController = Ember['default'].Controller.extend(LoginControllerMixin['default'], EmberValidations['default'].Mixin, {
    init: function() {
      this._super();
      return this.set("validations.identification.format.message", this.t('auth_forms.email_validate'));
    },
    needs: ["application"],
    authenticator: 'simple-auth-authenticator:devise',
    actions: {
      authenticate: function() {
        this.send("loading");
        return this._super().then((function(_this) {
          return function() {
            _this.send("finished");
            return _this.send("closeModal");
          };
        })(this), (function(_this) {
          return function() {
            return _this.send("finished");
          };
        })(this));
      },
      closeModal: function() {
        return true;
      }
    }
  });

  LoginController.reopen({
    identificationHasValue: (function() {
      var _ref;
      return !((_ref = this.get('identification')) != null ? _ref.length : void 0);
    }).property('identification'),
    submitClasses: (function() {
      if (this.get("isValid")) {
        return "button right";
      } else {
        return "button right disabled";
      }
    }).property("isValid"),
    validations: {
      identification: {
        presence: true,
        format: {
          "with": /@/,
          allowBlank: true
        }
      },
      password: {
        presence: true,
        length: {
          minimum: 6,
          allowBlank: true
        }
      }
    }
  });

  exports['default'] = LoginController;

});
define('outdoor-client/controllers/routes/new', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  var RoutesNewController;

  RoutesNewController = Ember['default'].Controller.extend({
    needs: ["application"],
    isCountryChoosed: false,
    isRegionChoosed: false,
    isSubregionChoosed: false,
    isCountryPropertiesChanged: false,
    isRegionPropertiesChanged: false,
    isSubregionPropertiesChanged: false,
    country: null,
    region: null,
    subregion: null,
    current: "country",
    countryState: (function() {
      if (this.get("current") === "country") {
        return "active";
      }
    }).property("current"),
    regionState: (function() {
      if (this.get("current") === "region") {
        return "active";
      }
    }).property("current"),
    subregionState: (function() {
      if (this.get("current") === "subregion") {
        return "active";
      }
    }).property("current"),
    actions: {
      onCountry: function() {
        if (this.get("isCountryChoosed") || this.get("current") === "country") {
          return this.transitionToRoute("routes.new.country");
        }
      },
      onRegion: function() {
        if (this.get("isRegionChoosed") || this.get("current") === "region") {
          return this.transitionToRoute("routes.new.region");
        } else {
          this.set("controllers.application.modalContent", "You must choose COUNTRY first!");
          return this.send("openModal");
        }
      },
      onSubregion: function() {
        if (this.get("isSubregionChoosed") || this.get("current") === "subregion") {
          return this.transitionToRoute("routes.new.subregion");
        } else {
          this.set("controllers.application.modalContent", "You must choose COUNTRY and REGION first!");
          return this.send("openModal");
        }
      }
    }
  });

  exports['default'] = RoutesNewController;

});
define('outdoor-client/controllers/routes/new/country', ['exports', 'ember', 'outdoor-client/mixins/choose-location-controller', 'outdoor-client/mixins/manage-typed-insertions-controller'], function (exports, Ember, ChooseLocationControllerMixin, ManageTypedInsertionsControllerMixin) {

  'use strict';

  var RoutesNewCountryController;

  RoutesNewCountryController = Ember['default'].Controller.extend(ChooseLocationControllerMixin['default'], ManageTypedInsertionsControllerMixin['default'], {
    locationType: "country",
    locatonPropertyName: "location"
  });

  exports['default'] = RoutesNewCountryController;

});
define('outdoor-client/controllers/routes/new/country/create', ['exports', 'ember', 'outdoor-client/mixins/create-location-controller', 'outdoor-client/mixins/manage-typed-insertions-controller'], function (exports, Ember, CreateLocationControllerMixin, ManageTypedInsertionsControllerMixin) {

  'use strict';

  var RoutesNewCountryCreateController;

  RoutesNewCountryCreateController = Ember['default'].Controller.extend(CreateLocationControllerMixin['default'], ManageTypedInsertionsControllerMixin['default'], {
    locationType: "country",
    locationsList: null
  });

  exports['default'] = RoutesNewCountryCreateController;

});
define('outdoor-client/controllers/routes/new/region', ['exports', 'ember', 'outdoor-client/mixins/choose-location-controller', 'outdoor-client/mixins/manage-typed-insertions-controller'], function (exports, Ember, ChooseLocationControllerMixin, ManageTypedInsertionsControllerMixin) {

  'use strict';

  var RoutesNewRegionController;

  RoutesNewRegionController = Ember['default'].Controller.extend(ChooseLocationControllerMixin['default'], ManageTypedInsertionsControllerMixin['default'], {
    locationType: "region",
    locatonPropertyName: "location"
  });

  exports['default'] = RoutesNewRegionController;

});
define('outdoor-client/controllers/routes/new/region/create', ['exports', 'ember', 'outdoor-client/mixins/create-location-controller', 'outdoor-client/mixins/manage-typed-insertions-controller'], function (exports, Ember, CreateLocationControllerMixin, ManageTypedInsertionsControllerMixin) {

  'use strict';

  var RoutesNewRegionCreateController;

  RoutesNewRegionCreateController = Ember['default'].Controller.extend(CreateLocationControllerMixin['default'], ManageTypedInsertionsControllerMixin['default'], {
    locationType: "region"
  });

  exports['default'] = RoutesNewRegionCreateController;

});
define('outdoor-client/controllers/routes/new/subregion', ['exports', 'ember', 'outdoor-client/mixins/choose-location-controller', 'outdoor-client/mixins/manage-typed-insertions-controller'], function (exports, Ember, ChooseLocationControllerMixin, ManageTypedInsertionsControllerMixin) {

  'use strict';

  var RoutesNewSubregionController;

  RoutesNewSubregionController = Ember['default'].Controller.extend(ChooseLocationControllerMixin['default'], ManageTypedInsertionsControllerMixin['default'], {
    locationType: "subregion",
    locatonPropertyName: "location"
  });

  exports['default'] = RoutesNewSubregionController;

});
define('outdoor-client/controllers/routes/new/subregion/create', ['exports', 'ember', 'outdoor-client/mixins/create-location-controller', 'outdoor-client/mixins/manage-typed-insertions-controller'], function (exports, Ember, CreateLocationControllerMixin, ManageTypedInsertionsControllerMixin) {

  'use strict';

  var RoutesNewSubregionCreateController;

  RoutesNewSubregionCreateController = Ember['default'].Controller.extend(CreateLocationControllerMixin['default'], ManageTypedInsertionsControllerMixin['default'], {
    locationType: "subregion",
    parentLocationType: "region"
  });

  exports['default'] = RoutesNewSubregionCreateController;

});
define('outdoor-client/helpers/em-capitalize', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  var EmCapitalizeHelper, emCapitalize;

  exports.emCapitalize = emCapitalize = function(value) {
    return value.toUpperCase();
  };

  EmCapitalizeHelper = Ember['default'].Handlebars.makeBoundHelper(emCapitalize);

  exports['default'] = EmCapitalizeHelper;

});
define('outdoor-client/helpers/t', ['exports', 'ember-cli-i18n/utils/stream'], function (exports, Stream) {

  'use strict';

  function tHelper(params, hash, options, env) {
    var view = env.data.view;
    var path = params.shift();

    var container = view.container;
    var t = container.lookup('utils:t');
    var application = container.lookup('application:main');

    var stream = new Stream['default'](function() {
      return t(path, params);
    });

    // bind any arguments that are Streams
    for (var i = 0, l = params.length; i < l; i++) {
      var param = params[i];
      if(param && param.isStream){
        param.subscribe(stream.notify, stream);
      };
    }

    application.localeStream.subscribe(stream.notify, stream);

    if (path.isStream) {
      path.subscribe(stream.notify, stream);
    }

    return stream;
  }
  exports['default'] = tHelper;

});
define('outdoor-client/initializers/current-user-service', ['exports'], function (exports) {

  'use strict';

  var CurrentUserInitializer;

  CurrentUserInitializer = {
    name: 'current-user-service',
    initialize: function(container, app) {}
  };

  exports['default'] = CurrentUserInitializer;

});
define('outdoor-client/initializers/export-application-global', ['exports', 'ember', 'outdoor-client/config/environment'], function (exports, Ember, config) {

  'use strict';

  exports.initialize = initialize;

  function initialize(container, application) {
    var classifiedName = Ember['default'].String.classify(config['default'].modulePrefix);

    if (config['default'].exportApplicationGlobal && !window[classifiedName]) {
      window[classifiedName] = application;
    }
  };

  exports['default'] = {
    name: 'export-application-global',

    initialize: initialize
  };

});
define('outdoor-client/initializers/google-map', ['exports', 'outdoor-client/utils/load-google-map'], function (exports, loadGoogleMap) {

  'use strict';

  var GoogleMapInitializer, initialize;

  exports.initialize = initialize = function(container, app) {
    app.register('util:load-google-map', loadGoogleMap['default'], {
      instantiate: false
    });
    return app.inject('route', 'loadGoogleMap', 'util:load-google-map');
  };

  GoogleMapInitializer = {
    name: 'google-map',
    initialize: initialize
  };

  exports['default'] = GoogleMapInitializer;

});
define('outdoor-client/initializers/simple-auth-devise', ['exports', 'simple-auth-devise/configuration', 'simple-auth-devise/authenticators/devise', 'simple-auth-devise/authorizers/devise', 'outdoor-client/config/environment'], function (exports, Configuration, Authenticator, Authorizer, ENV) {

  'use strict';

  exports['default'] = {
    name:       'simple-auth-devise',
    before:     'simple-auth',
    initialize: function(container, application) {
      Configuration['default'].load(container, ENV['default']['simple-auth-devise'] || {});
      container.register('simple-auth-authorizer:devise', Authorizer['default']);
      container.register('simple-auth-authenticator:devise', Authenticator['default']);
    }
  };

});
define('outdoor-client/initializers/simple-auth', ['exports', 'simple-auth/configuration', 'simple-auth/setup', 'outdoor-client/config/environment'], function (exports, Configuration, setup, ENV) {

  'use strict';

  exports['default'] = {
    name:       'simple-auth',
    initialize: function(container, application) {
      Configuration['default'].load(container, ENV['default']['simple-auth'] || {});
      setup['default'](container, application);
    }
  };

});
define('outdoor-client/initializers/t', ['exports', 'ember', 'ember-cli-i18n/utils/t', 'outdoor-client/helpers/t', 'ember-cli-i18n/utils/stream'], function (exports, Ember, T, tHelper, Stream) {

  'use strict';

  exports.initialize = initialize;

  function initialize(container, application) {
    Ember['default'].HTMLBars._registerHelper('t', tHelper['default']);

    application.localeStream = new Stream['default'](function() {
      return  application.get('locale');
    });

    Ember['default'].addObserver(application, 'locale', function() {
      application.localeStream.notify();
    });

    application.register('utils:t', T['default']);
    application.inject('route', 't', 'utils:t');
    application.inject('model', 't', 'utils:t');
    application.inject('component', 't', 'utils:t');
    application.inject('controller', 't', 'utils:t');
  };

  exports['default'] = {
    name: 't',
    initialize: initialize
  };

});
define('outdoor-client/initializers/translation-to-service', ['exports'], function (exports) {

  'use strict';

  var TranslationToServiceInitializer, initialize;

  initialize = function(container, app) {
    var defaultLocale;
    app.deferReadiness();
    defaultLocale = app.defaultLocale;
    return Ember.$.getJSON("/locales/" + defaultLocale + ".json", function(translation) {
      var tr;
      tr = Ember.K;
      tr.translation = translation;
      app.register('i18n:translation', tr, {
        instantiate: false
      });
      return app.advanceReadiness();
    });
  };

  TranslationToServiceInitializer = {
    name: 'translation-to-service',
    initialize: initialize
  };

  exports['default'] = TranslationToServiceInitializer;

});
define('outdoor-client/mixins/authenticated-route', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  var AuthenticatedRouteMixin;

  AuthenticatedRouteMixin = Ember['default'].Mixin.create({
    ability: Ember['default'].K,
    roles: ["user", "moderator", "admin"],
    roleIsCan: function(base_role) {
      var roles;
      if (this.session.get("isAuthenticated") != null) {
        roles = this.get("roles");
        if (roles.indexOf(base_role) <= roles.indexOf(this.session.get("content.role"))) {
          return true;
        } else {
          this.send("error", new Ember['default'].Error(this.t('errors.no_page_permission')));
          return false;
        }
      } else {
        this.send("login");
        return false;
      }
    },
    beforeModel: function(transition) {
      if (!this.roleIsCan(this.get("ability"))) {
        transition.abort();
        this.transitionTo("root");
      }
      return this._super();
    }
  });

  exports['default'] = AuthenticatedRouteMixin;

});
define('outdoor-client/mixins/choose-location-controller', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  var ChooseLocationControllerMixin;

  ChooseLocationControllerMixin = Ember['default'].Mixin.create({
    needs: "routes/new",
    locationType: null,
    selectPrompt: (function() {
      return this.t("routes.new.choose_" + this.get("locationType") + "_ph");
    }).property(),
    isLocationChoosed: false,
    isNewLocation: false,
    isPropertiesChanged: false,
    isCountry: (function() {
      return this.get("locationType") === "country";
    }).property(),
    isRegion: (function() {
      return this.get("locationType") === "region";
    }).property(),
    isSubregion: (function() {
      return this.get("locationType") === "subregion";
    }).property(),
    isPropertiesChangedObserver: (function() {
      var prop;
      prop = "controllers.routes/new.is" + this.get("locationType").charAt(0).toUpperCase() + this.get("locationType").substr(1) + "PropertiesChanged";
      if (this.get("isPropertiesChanged")) {
        return this.set(prop, true);
      } else {
        return this.set(prop, true);
      }
    }).observes("isPropertiesChanged"),
    locationId: null,
    location: null,
    isCurrentStep: (function() {
      return this.get("controllers.routes/new.current") === this.get("locationType");
    }).property("controllers.routes/new.current"),
    defaultLocationName: (function() {
      return this.get("model").findBy("name", this.get("location.name"));
    }).property("location"),
    nextClasses: (function() {
      if (this.get("isLocationChoosed")) {
        return "button";
      } else {
        return "button disabled";
      }
    }).property("isLocationChoosed"),
    actions: {
      createLocation: function() {
        this.set("isLocationChoosed", false);
        this.set("locationId", null);
        this.set("isNewLocation", true);
        return this.transitionToRoute("routes.new." + this.get("locationType") + ".create");
      },
      locationChoose: function(locationId) {
        if (this.get("isLocationChoosed")) {
          this.send("locationChoosed", this.get("locationType"), locationId);
        }
        return false;
      },
      propertyChanged: function() {
        this.set("isPropertiesChanged", true);
        return false;
      }
    }
  });

  exports['default'] = ChooseLocationControllerMixin;

});
define('outdoor-client/mixins/choose-location-route', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  var ChooseLocationRouteMixin;

  ChooseLocationRouteMixin = Ember['default'].Mixin.create({
    actions: {
      selectChanged: function(location_id) {
        this.send("loading");
        if (this.controller.get("isPropertiesChanged")) {
          this.controller.get("location").rollback();
          this.controller.set("isPropertiesChanged", false);
        }
        this.store.find(this.controller.get("locationType"), location_id).then((function(_this) {
          return function(location) {
            _this.controller.set("locationId", location_id);
            _this.controller.set("isLocationChoosed", true);
            _this.controller.set("location", location);
            return _this.send('finished');
          };
        })(this), (function(_this) {
          return function() {
            return _this.send('finished');
          };
        })(this));
        return false;
      }
    }
  });

  exports['default'] = ChooseLocationRouteMixin;

});
define('outdoor-client/mixins/create-location-controller', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  var CreateLocationControllerMixin;

  CreateLocationControllerMixin = Ember['default'].Mixin.create({
    locationType: "country",
    parentLocationType: "country",
    photosBinding: "model.gallery.albums.firstObject.photos",
    needs: (function() {
      var _this;
      _this = this;
      return ["routes/new/" + _this.get("locationType"), "routes/new"];
    }).property(),
    actions: {
      locationNameAdded: function(locationNameObj) {
        this.set("model.name", locationNameObj.name);
        if (locationNameObj.cca2 != null) {
          this.set("model.cca2", locationNameObj.cca2);
        }
        if (locationNameObj.cca3 != null) {
          this.set("model.cca3", locationNameObj.cca3);
        }
        this.set("model.latitude", locationNameObj.latitude);
        return this.set("model.longitude", locationNameObj.longitude);
      },
      createLocation: function() {
        var locationType, parentLocation, parentLocationType, photos;
        this.send("loading");
        locationType = this.get("locationType");
        parentLocationType = this.get("parentLocationType");
        if (parentLocationType !== locationType) {
          parentLocation = this.get("controllers.routes/new." + parentLocationType);
          this.set('model.' + parentLocationType, parentLocation);
        }
        photos = this.get("photos");
        return this.get('model').save().then((function(_this) {
          return function(location) {
            location.get("gallery.albums.firstObject.photos").addObjects(photos);
            location.get("gallery.albums.firstObject.photos").save();
            return _this.store.find(locationType + "-name", {
              parent_location_id: parentLocation ? parentLocation.get("id") : null
            }).then(function(location_names) {
              _this.set("controllers.routes/new/" + locationType + ".model", location_names);
              _this.set("controllers.routes/new/" + locationType + ".location", location);
              _this.set("controllers.routes/new/" + locationType + ".locationId", location.get("id"));
              _this.set("controllers.routes/new/" + locationType + ".isLocationChoosed", true);
              _this.send('finished');
              return _this.transitionToRoute("routes.new." + locationType);
            });
          };
        })(this), (function(_this) {
          return function(error) {
            return _this.send("error", error);
          };
        })(this));
      }
    }
  });

  exports['default'] = CreateLocationControllerMixin;

});
define('outdoor-client/mixins/create-location-route', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  var CreateLocationRouteMixin;

  CreateLocationRouteMixin = Ember['default'].Mixin.create({
    deactivate: function() {
      var model;
      model = this.controller.get("model");
      if (model.id == null) {
        model.deleteRecord();
      }
      this.send("closeModal");
      return this.controller.set("controllers.routes/new/" + this.controller.get("locationType") + ".isNewLocation", false);
    }
  });

  exports['default'] = CreateLocationRouteMixin;

});
define('outdoor-client/mixins/foundation', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  var FoundationMixin;

  FoundationMixin = Ember['default'].Mixin.create({
    didInsertElement: function() {
      this._super();
      return Ember['default'].run.scheduleOnce('afterRender', this, this.afterRenderElement);
    },
    afterRenderElement: function() {
      this.get("parentView").$().foundation();
      return $(document).foundation();
    }
  });

  exports['default'] = FoundationMixin;

});
define('outdoor-client/mixins/in-gmap', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  var InGmapMixin;

  InGmapMixin = Ember['default'].Mixin.create({
    gmapComponent: (function() {
      var parentView;
      parentView = this.get('parentView');
      while (parentView != null) {
        if (parentView.get('mapCanvas') != null) {
          return parentView;
        }
        parentView = parentView.get('parentView');
      }
      return Em.assert(false, 'Cannot find gm-map component');
    }).property('parentView')
  });

  exports['default'] = InGmapMixin;

});
define('outdoor-client/mixins/inject-scripts', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  var InjectScriptsMixin;

  InjectScriptsMixin = Ember['default'].Mixin.create({
    injectScripts: [],
    beforeModel: function() {
      var promises;
      promises = [];
      this.get("injectScripts").forEach(function(scriptSRC) {
        return promises.push(new Ember['default'].RSVP.Promise(function(resolve, reject) {
          var script;
          script = document.createElement('script');
          script.type = 'text/javascript';
          script.async = true;
          script.src = scriptSRC;
          script.onload = function() {
            console.log(scriptSRC);
            console.log("is loaded");
            return resolve();
          };
          script.onerror = function() {
            console.log(scriptSRC);
            console.log("is rejected");
            return reject();
          };
          return document.getElementsByTagName('head')[0].appendChild(script);
        }));
      });
      return new Ember['default'].RSVP.all(promises);
    }
  });

  exports['default'] = InjectScriptsMixin;

});
define('outdoor-client/mixins/loading-slider', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  exports['default'] = Ember['default'].Mixin.create({
    actions: {
      loading: function() {
        var controller = this.controllerFor('application');
        controller.set('loading', true);
        this.router.one('didTransition', function() {
          controller.set('loading', false);
        });
      },
      finished: function() {
        this.controllerFor('application').set('loading', false);
      }
    }
  });

});
define('outdoor-client/mixins/location-serializer', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  var LocationSerializerMixin;

  LocationSerializerMixin = Ember['default'].Mixin.create(DS.EmbeddedRecordsMixin, {
    attrs: {
      transportHubs: {
        serialize: 'records',
        deserialize: 'ids'
      },
      contacts: {
        serialize: 'records',
        deserialize: 'ids'
      },
      gallery: {
        embedded: 'always'
      }
    },
    keyForAttribute: function(attr) {
      switch (attr) {
        case "transportHubs":
          return "transport_hubs_attributes";
        case "contacts":
          return "contacts_attributes";
        case "accommodations":
          return "accommodations_attributes";
        case "gallery":
          return "gallery_attributes";
        default:
          return this._super(attr);
      }
    }
  });

  exports['default'] = LocationSerializerMixin;

});
define('outdoor-client/mixins/manage-typed-insertions-controller', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  var ManageTypedInsertionsControllerMixin;

  ManageTypedInsertionsControllerMixin = Ember['default'].Mixin.create({
    typedInsertion: null,
    confirm: null,
    locatonPropertyName: "model",
    transportHubTypes: ["airport", "seaport", "railway", "bus", "taxi", "hitchhiking", "public", "other"],
    accommodationTypes: ["hotel", "guesthouse", "wildcamping", "other"],
    file: null,
    dataURL: null,
    actions: {
      addInsertion: function(typeKey) {
        this.set("typedInsertion", this.store.createRecord(typeKey));
        this.send("openModal");
        return true;
      },
      createTransportHub: function() {
        var lpn;
        lpn = this.get("locatonPropertyName");
        this.get(lpn).get("transportHubs").addObject(this.get("typedInsertion"));
        return this.send("closeModal");
      },
      createContact: function() {
        var lpn;
        lpn = this.get("locatonPropertyName");
        this.get(lpn).get("contacts").addObject(this.get("typedInsertion"));
        return this.send("closeModal");
      },
      createAccommodation: function() {
        var lpn;
        lpn = this.get("locatonPropertyName");
        this.get(lpn).get("accommodations").addObject(this.get("typedInsertion"));
        return this.send("closeModal");
      },
      createPhoto: function() {
        var album, lpn, photo;
        lpn = this.get("locatonPropertyName");
        album = this.get(lpn).get("gallery.albums.firstObject");
        photo = this.get("typedInsertion");
        photo.set("image", this.get("file"));
        photo.set("dataURL", this.get("dataURL"));
        album.get("photos").addObject(photo);
        this.set("dataURL", null);
        return this.send("closeModal");
      },
      destroyInsertion: function(insertion) {
        this.set("confirm", insertion);
        this.send("openModal");
        return true;
      },
      confirmed: function(insertion) {
        var insertionTypeKey, lpn;
        this.send("closeModal");
        insertionTypeKey = insertion.constructor.typeKey;
        lpn = this.get("locatonPropertyName");
        return this.get(lpn).get(insertionTypeKey + "s").removeObject(insertion);
      },
      cancelConfirm: function() {
        this.set("confirm", null);
        return this.send("closeModal");
      },
      editInsertion: function(insertion) {
        this.set("typedInsertion", insertion);
        this.send("openModal");
        return true;
      },
      insertionSelectChanged: function(insertionType) {
        return this.set("typedInsertion.insertionType", insertionType);
      }
    }
  });

  exports['default'] = ManageTypedInsertionsControllerMixin;

});
define('outdoor-client/mixins/manage-typed-insertions-route', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  var ManageTypedInsertionsRouteMixin;

  ManageTypedInsertionsRouteMixin = Ember['default'].Mixin.create({
    actions: {
      addInsertion: function(typeKey) {
        this.render('typed-insertion/add' + typeKey.charAt(0).toUpperCase() + typeKey.substr(1), {
          into: "application",
          outlet: 'modal'
        });
        return false;
      },
      destroyInsertion: function() {
        this.render('confirm', {
          into: "application",
          outlet: 'modal'
        });
        return false;
      },
      editInsertion: function(insertion) {
        this.send("addInsertion", insertion.constructor.typeKey);
        return false;
      }
    }
  });

  exports['default'] = ManageTypedInsertionsRouteMixin;

});
define('outdoor-client/mixins/typed-insertion', ['exports', 'ember', 'ember-data'], function (exports, Ember, DS) {

  'use strict';

  var TypedInsertionMixin;

  TypedInsertionMixin = Ember['default'].Mixin.create({
    insertionType: DS['default'].attr("string"),
    name: DS['default'].attr("string"),
    description: DS['default'].attr("string")
  });

  exports['default'] = TypedInsertionMixin;

});
define('outdoor-client/models/accommodation', ['exports', 'ember', 'ember-data', 'outdoor-client/mixins/typed-insertion', 'ember-validations'], function (exports, Ember, DS, TypedInsertionMixin, EmberValidations) {

  'use strict';

  var Accommodation;

  Accommodation = DS['default'].Model.extend(TypedInsertionMixin['default'], EmberValidations['default'].Mixin, {
    accommodationType: Ember['default'].computed.alias("insertionType")
  });

  Accommodation.reopen({
    validations: {
      accommodationType: {
        presence: true,
        inclusion: {
          "in": ["hotel", "guesthouse", "wildcamping", "other"]
        }
      },
      name: {
        presence: true
      },
      description: {
        presence: true
      }
    }
  });

  exports['default'] = Accommodation;

});
define('outdoor-client/models/album', ['exports', 'ember-data'], function (exports, DS) {

  'use strict';

  var Album;

  Album = DS['default'].Model.extend({
    name: DS['default'].attr("string"),
    description: DS['default'].attr("string"),
    gallery: DS['default'].belongsTo('gallery'),
    photos: DS['default'].hasMany('photo')
  });

  exports['default'] = Album;

});
define('outdoor-client/models/contact', ['exports', 'ember', 'ember-data', 'outdoor-client/mixins/typed-insertion', 'ember-validations'], function (exports, Ember, DS, TypedInsertionMixin, EmberValidations) {

  'use strict';

  var Contact;

  Contact = DS['default'].Model.extend(TypedInsertionMixin['default'], EmberValidations['default'].Mixin, {
    phone: Ember['default'].computed.alias("name"),
    insertionType: DS['default'].attr("string", {
      defaultValue: "phone"
    })
  });

  Contact.reopen({
    validations: {
      phone: {
        presence: true
      },
      description: {
        presence: true
      }
    }
  });

  exports['default'] = Contact;

});
define('outdoor-client/models/country-name', ['exports', 'ember-data'], function (exports, DS) {

  'use strict';

  var CountryName;

  CountryName = DS['default'].Model.extend({
    name: DS['default'].attr("string"),
    latitude: DS['default'].attr("number"),
    longitude: DS['default'].attr("number"),
    cca2: DS['default'].attr("string"),
    cca3: DS['default'].attr("string")
  });

  exports['default'] = CountryName;

});
define('outdoor-client/models/country', ['exports', 'ember-data', 'ember-validations'], function (exports, DS, EmberValidations) {

  'use strict';

  var Country;

  Country = DS['default'].Model.extend(EmberValidations['default'].Mixin, {
    name: DS['default'].attr("string"),
    cca2: DS['default'].attr("string"),
    cca3: DS['default'].attr("string"),
    latitude: DS['default'].attr("number"),
    longitude: DS['default'].attr("number"),
    description: DS['default'].attr("string"),
    visaInfo: DS['default'].attr("string"),
    governmentPage: DS['default'].attr("string"),
    transportInfo: DS['default'].attr("string"),
    regions: DS['default'].hasMany('region', {
      async: true
    }),
    transportHubs: DS['default'].hasMany("transportHub", {
      async: true
    }),
    contacts: DS['default'].hasMany('contact', {
      async: true
    }),
    gallery: DS['default'].belongsTo("gallery")
  });

  Country.reopen({
    validations: {
      name: {
        presence: true
      },
      governmentPage: {
        format: {
          "with": /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/,
          allowBlank: false,
          message: 'must be url path'
        }
      }
    }
  });

  exports['default'] = Country;

});
define('outdoor-client/models/gallery', ['exports', 'ember-data'], function (exports, DS) {

  'use strict';

  var Gallery;

  Gallery = DS['default'].Model.extend({
    albums: DS['default'].hasMany('album')
  });

  exports['default'] = Gallery;

});
define('outdoor-client/models/photo', ['exports', 'ember-data', 'ember-validations'], function (exports, DS, EmberValidations) {

  'use strict';

  var Photo;

  Photo = DS['default'].Model.extend(EmberValidations['default'].Mixin, {
    name: DS['default'].attr("string"),
    description: DS['default'].attr("string"),
    image: DS['default'].attr("file"),
    dataURL: DS['default'].attr("string"),
    image_random_thumb: DS['default'].attr("string"),
    albums: DS['default'].hasMany('album')
  });

  Photo.reopen({
    validations: {
      image: {
        presence: true
      }
    }
  });

  exports['default'] = Photo;

});
define('outdoor-client/models/region-name', ['exports', 'ember-data'], function (exports, DS) {

  'use strict';

  var RegionName;

  RegionName = DS['default'].Model.extend({
    name: DS['default'].attr("string"),
    latitude: DS['default'].attr("number"),
    longitude: DS['default'].attr("number")
  });

  exports['default'] = RegionName;

});
define('outdoor-client/models/region', ['exports', 'ember-data', 'ember-validations'], function (exports, DS, EmberValidations) {

  'use strict';

  var Region;

  Region = DS['default'].Model.extend(EmberValidations['default'].Mixin, {
    name: DS['default'].attr("string"),
    description: DS['default'].attr("string"),
    latitude: DS['default'].attr("number"),
    longitude: DS['default'].attr("number"),
    country: DS['default'].belongsTo('country', {
      async: true
    }),
    subregions: DS['default'].hasMany('subregion', {
      async: true
    }),
    transportHubs: DS['default'].hasMany("transportHub", {
      async: true
    }),
    contacts: DS['default'].hasMany('contact', {
      async: true
    })
  });

  Region.reopen({
    validations: {
      name: {
        presence: true
      }
    }
  });

  exports['default'] = Region;

});
define('outdoor-client/models/subregion-name', ['exports', 'ember-data'], function (exports, DS) {

  'use strict';

  var SubregionName;

  SubregionName = DS['default'].Model.extend({
    name: DS['default'].attr("string"),
    latitude: DS['default'].attr("number"),
    longitude: DS['default'].attr("number")
  });

  exports['default'] = SubregionName;

});
define('outdoor-client/models/subregion', ['exports', 'ember-data', 'ember-validations'], function (exports, DS, EmberValidations) {

  'use strict';

  var Subregion;

  Subregion = DS['default'].Model.extend(EmberValidations['default'].Mixin, {
    name: DS['default'].attr("string"),
    description: DS['default'].attr("string"),
    latitude: DS['default'].attr("number"),
    longitude: DS['default'].attr("number"),
    region: DS['default'].belongsTo('region', {
      async: true
    }),
    transportHubs: DS['default'].hasMany("transportHub", {
      async: true
    }),
    contacts: DS['default'].hasMany('contact', {
      async: true
    }),
    accommodations: DS['default'].hasMany('accommodation', {
      async: true
    })
  });

  Subregion.reopen({
    validations: {
      name: {
        presence: true
      }
    }
  });

  exports['default'] = Subregion;

});
define('outdoor-client/models/transport-hub', ['exports', 'ember', 'ember-data', 'outdoor-client/mixins/typed-insertion', 'ember-validations'], function (exports, Ember, DS, TypedInsertionMixin, EmberValidations) {

  'use strict';

  var TransportHub;

  TransportHub = DS['default'].Model.extend(TypedInsertionMixin['default'], EmberValidations['default'].Mixin, {
    hubType: Ember['default'].computed.alias("insertionType")
  });

  TransportHub.reopen({
    validations: {
      hubType: {
        presence: true,
        inclusion: {
          "in": ["airport", "seaport", "railway", "bus", "taxi", "hitchhiking", "public", "other"]
        }
      },
      name: {
        presence: true
      },
      description: {
        presence: true
      }
    }
  });

  exports['default'] = TransportHub;

});
define('outdoor-client/models/user', ['exports', 'ember-data'], function (exports, DS) {

  'use strict';

  var User;

  User = DS['default'].Model.extend({
    email: DS['default'].attr("string"),
    role: DS['default'].attr("string"),
    lang: DS['default'].attr("string"),
    chrole: function(base_role) {
      var roles;
      roles = ["user", "moderator", "admin"];
      return roles.indexOf("base_role") <= roles.indexOf(this.get("role"));
    },
    isUser: (function() {
      return this.get("session").isAuthenticated;
    }).property("session.isAuthenticated"),
    isModerator: (function() {
      return this.chrole("moderator");
    }).property("session.isAuthenticated"),
    isAdmin: (function() {
      return this.chrole("admin");
    }).property("session.isAuthenticated")
  });

  exports['default'] = User;

});
define('outdoor-client/router', ['exports', 'ember', 'outdoor-client/config/environment'], function (exports, Ember, config) {

  'use strict';

  var Router;

  Router = Ember['default'].Router.extend({
    location: config['default'].locationType
  });

  Router.map(function() {
    return this.resource("routes", function() {
      return this.resource("routes.new", {
        path: '/new'
      }, function() {
        this.resource("routes.new.country", {
          path: '/country'
        }, function() {
          return this.route("create");
        });
        this.resource("routes.new.region", {
          path: '/region'
        }, function() {
          return this.route("create");
        });
        return this.resource("routes.new.subregion", {
          path: '/subregion'
        }, function() {
          return this.route("create");
        });
      });
    });
  });

  exports['default'] = Router;

});
define('outdoor-client/routes/application', ['exports', 'ember', 'simple-auth/mixins/application-route-mixin', 'outdoor-client/mixins/loading-slider'], function (exports, Ember, ApplicationRouteMixin, LoadingSliderMixin) {

  'use strict';

  var ApplicationRoute;

  ApplicationRoute = Ember['default'].Route.extend(ApplicationRouteMixin['default'], LoadingSliderMixin['default'], {
    beforeModel: function() {
      var application, lang, set;
      this._super();
      lang = this.session.get("content.lang");
      if (lang != null) {
        application = this.container.lookup('application:main');
        if (application.defaultLocale !== lang) {
          set = Ember['default'].set;
          set(application, 'defaultLocale', lang);
          this.controller.set("lang", lang);
          return Ember['default'].$.getJSON("/locales/" + application.defaultLocale + ".json", (function(_this) {
            return function(translation) {
              return _this.container.lookup('i18n:translation').translation = translation;
            };
          })(this));
        }
      }
    },
    actions: {
      login: function() {
        if (!this.session.isAuthenticated) {
          this.render("login", {
            into: "application",
            outlet: 'modal',
            controller: "login"
          });
          return this.send("openModal");
        }
      },
      error: function(error, transition) {
        switch (error.status) {
          case 401:
            this.send("login");
            break;
          case 403:
            this.controller.set("modalContent", "You do not have sufficient permissions to access this page!");
            this.send("openModal");
        }
        return this._super(error, transition);
      },
      closeModal: function() {
        this.controller.set("isOpenModal", false);
        this.controller.set("modalContent", "");
        return this.disconnectOutlet({
          outlet: 'modal',
          parentView: 'application'
        });
      },
      openModal: function() {
        return this.controller.set("isOpenModal", true);
      },
      changeLang: function(lang) {
        var application;
        application = this.container.lookup('application:main');
        if (application.defaultLocale !== lang) {
          Foundation.utils.S("li#" + application.defaultLocale).removeClass("active");
          Foundation.utils.S("li#" + lang).addClass("active");
          this.controller.set("lang", lang);
          return Ember['default'].$.getJSON("/locales/" + lang + ".json", (function(_this) {
            return function(translation) {
              var set;
              _this.container.lookup('i18n:translation').translation = translation;
              set = Ember['default'].set;
              set(application, 'defaultLocale', lang);
              set(application, 'locale', lang);
              return _this.session.set("content.lang", lang);
            };
          })(this));
        }
      }
    }
  });

  exports['default'] = ApplicationRoute;

});
define('outdoor-client/routes/routes/new', ['exports', 'ember', 'outdoor-client/mixins/authenticated-route'], function (exports, Ember, AuthenticatedRouteMixin) {

  'use strict';

  var RoutesNewRoute;

  RoutesNewRoute = Ember['default'].Route.extend(AuthenticatedRouteMixin['default'], {
    ability: "user",
    isCountryChoosed: Ember['default'].computed.alias("controller.isCountryChoosed"),
    isRegionChoosed: Ember['default'].computed.alias("controller.isRegionChoosed"),
    isSubregionChoosed: Ember['default'].computed.alias("controller.isSubregionChoosed"),
    isCountryPropertiesChanged: Ember['default'].computed.alias("controller.isCountryPropertiesChanged"),
    isRegionPropertiesChanged: Ember['default'].computed.alias("controller.isRegionPropertiesChanged"),
    isSubregionPropertiesChanged: Ember['default'].computed.alias("controller.isSubregionsPropertiesChanged"),
    countryId: null,
    regionId: null,
    subregionId: null,
    country: Ember['default'].computed.alias("controller.country"),
    region: Ember['default'].computed.alias("controller.region"),
    subregion: Ember['default'].computed.alias("controller.subregion"),
    current: Ember['default'].computed.alias("controller.current"),
    chooseLocation: function(locationKey, isChoosed) {
      return this.set("is" + locationKey.charAt(0).toUpperCase() + locationKey.substr(1) + "Choosed", isChoosed);
    },
    clearLocation: function(locationKey) {
      this.set(locationKey, null);
      return this.chooseLocation(locationKey, false);
    },
    loadLocation: function(locationKey) {
      this.send("loading");
      return this.store.find(locationKey, this.get(locationKey + "Id")).then((function(_this) {
        return function(location) {
          _this.set(locationKey, location);
          _this.chooseLocation(locationKey, true);
          return _this.send('finished');
        };
      })(this), (function(_this) {
        return function() {
          return _this.send('finished');
        };
      })(this));
    },
    countryIdObserve: (function() {
      if (this.get("countryId")) {
        return this.loadLocation("country");
      } else {
        return this.clearLocation("country");
      }
    }).observes("countryId"),
    regionIdObserve: (function() {
      if (this.get("regionId")) {
        return this.loadLocation("region");
      } else {
        return this.clearLocation("region");
      }
    }).observes("regionId"),
    subregionIdObserve: (function() {
      if (this.get("subregionId")) {
        return this.loadLocation("subregion");
      } else {
        return this.clearLocation("subregion");
      }
    }).observes("subregionId"),
    afterModel: function() {
      return this.transitionTo("routes.new.country");
    },
    actions: {
      locationChoosed: function(locationName, locationId) {
        this.set(locationName + "Id", locationId);
        switch (locationName) {
          case "country":
            this.set("regionId", null);
            this.set("subregionId", null);
            this.set("current", "region");
            return this.transitionTo("routes.new.region");
          case "region":
            this.set("subregionId", null);
            this.set("current", "subregion");
            return this.transitionTo("routes.new.subregion");
          case "subregion":
            this.set("current", "");
            if (this.get("isCountryChoosed") && this.get("isRegionChoosed") && this.get("isSubregionChoosed")) {
              return alert("OK!");
            }
        }
      }
    }
  });

  exports['default'] = RoutesNewRoute;

});
define('outdoor-client/routes/routes/new/country', ['exports', 'ember', 'outdoor-client/mixins/choose-location-route', 'outdoor-client/mixins/manage-typed-insertions-route'], function (exports, Ember, ChooseLocationRouteMixin, ManageTypedInsertionsRouteMixin) {

  'use strict';

  var RoutesNewCountryRoute;

  RoutesNewCountryRoute = Ember['default'].Route.extend(ChooseLocationRouteMixin['default'], ManageTypedInsertionsRouteMixin['default'], {
    model: function(params, transition) {
      return this.store.find("country-name");
    }
  });

  exports['default'] = RoutesNewCountryRoute;

});
define('outdoor-client/routes/routes/new/country/create', ['exports', 'ember', 'outdoor-client/mixins/create-location-route', 'outdoor-client/mixins/manage-typed-insertions-route'], function (exports, Ember, CreateLocationRouteMixin, ManageTypedInsertionsRouteMixin) {

  'use strict';

  var RoutesNewCountryCreateRoute;

  RoutesNewCountryCreateRoute = Ember['default'].Route.extend(CreateLocationRouteMixin['default'], ManageTypedInsertionsRouteMixin['default'], {
    model: function(params, transition) {
      return Ember['default'].$.getJSON("/countries_list");
    },
    setupController: function(controller, tempModel) {
      var model;
      model = this.store.createRecord('country');
      model.set("gallery", this.store.createRecord('gallery'));
      model.get("gallery.albums").addObject(this.store.createRecord('album'));
      controller.set("locationsList", tempModel);
      return controller.set('model', model);
    }
  });

  exports['default'] = RoutesNewCountryCreateRoute;

});
define('outdoor-client/routes/routes/new/region', ['exports', 'ember', 'outdoor-client/mixins/choose-location-route', 'outdoor-client/mixins/manage-typed-insertions-route'], function (exports, Ember, ChooseLocationRouteMixin, ManageTypedInsertionsRouteMixin) {

  'use strict';

  var RoutesNewRegionRoute;

  RoutesNewRegionRoute = Ember['default'].Route.extend(ChooseLocationRouteMixin['default'], ManageTypedInsertionsRouteMixin['default'], {
    model: function() {
      var country_id;
      this.container.lookup('controller:routes.new');
      country_id = this.container.lookup('controller:routes.new').get("country.id");
      return this.store.find("region-name", {
        parent_location_id: country_id
      });
    }
  });

  exports['default'] = RoutesNewRegionRoute;

});
define('outdoor-client/routes/routes/new/region/create', ['exports', 'ember', 'outdoor-client/mixins/create-location-route', 'outdoor-client/mixins/manage-typed-insertions-route'], function (exports, Ember, CreateLocationRouteMixin, ManageTypedInsertionsRouteMixin) {

  'use strict';

  var RoutesNewRegionCreateRoute;

  RoutesNewRegionCreateRoute = Ember['default'].Route.extend(CreateLocationRouteMixin['default'], ManageTypedInsertionsRouteMixin['default'], {
    model: function(params, transition) {
      return this.store.createRecord('region');
    }
  });

  exports['default'] = RoutesNewRegionCreateRoute;

});
define('outdoor-client/routes/routes/new/subregion', ['exports', 'ember', 'outdoor-client/mixins/choose-location-route', 'outdoor-client/mixins/manage-typed-insertions-route'], function (exports, Ember, ChooseLocationRouteMixin, ManageTypedInsertionsRouteMixin) {

  'use strict';

  var RoutesNewSubregionRoute;

  RoutesNewSubregionRoute = Ember['default'].Route.extend(ChooseLocationRouteMixin['default'], ManageTypedInsertionsRouteMixin['default'], {
    setupController: function(controller, model) {
      var region_id;
      region_id = controller.get("controllers.routes/new.region.id");
      model = this.store.find("subregion-name", {
        parent_location_id: region_id
      });
      return controller.set('model', model);
    }
  });

  exports['default'] = RoutesNewSubregionRoute;

});
define('outdoor-client/routes/routes/new/subregion/create', ['exports', 'ember', 'outdoor-client/mixins/create-location-route', 'outdoor-client/mixins/manage-typed-insertions-route'], function (exports, Ember, CreateLocationRouteMixin, ManageTypedInsertionsRouteMixin) {

  'use strict';

  var RoutesNewSubegionCreateRoute;

  RoutesNewSubegionCreateRoute = Ember['default'].Route.extend(CreateLocationRouteMixin['default'], ManageTypedInsertionsRouteMixin['default'], {
    model: function(params, transition) {
      return this.store.createRecord('subregion');
    }
  });

  exports['default'] = RoutesNewSubegionCreateRoute;

});
define('outdoor-client/serializers/album', ['exports', 'ember-data'], function (exports, DS) {

  'use strict';

  var AlbumSerializer;

  AlbumSerializer = DS['default'].ActiveModelSerializer.extend(DS['default'].EmbeddedRecordsMixin, {
    attrs: {
      photos: {
        serialize: false,
        deserialize: 'ids'
      },
      gallery: {
        serialize: false,
        deserialize: 'ids'
      }
    },
    keyForAttribute: function(attr) {
      switch (attr) {
        case "photos":
          return "photos_attributes";
        default:
          return this._super(attr);
      }
    }
  });

  exports['default'] = AlbumSerializer;

});
define('outdoor-client/serializers/country', ['exports', 'ember-data', 'outdoor-client/mixins/location-serializer'], function (exports, DS, LocationSerializerMixin) {

	'use strict';

	var CountrySerializer;

	CountrySerializer = DS['default'].ActiveModelSerializer.extend(DS['default'].EmbeddedRecordsMixin, LocationSerializerMixin['default'], {});

	exports['default'] = CountrySerializer;

});
define('outdoor-client/serializers/gallery', ['exports', 'ember-data'], function (exports, DS) {

  'use strict';

  var GallerySerializer;

  GallerySerializer = DS['default'].ActiveModelSerializer.extend(DS['default'].EmbeddedRecordsMixin, {
    attrs: {
      albums: {
        embedded: 'always'
      }
    },
    keyForAttribute: function(attr) {
      switch (attr) {
        case "albums":
          return "albums_attributes";
        default:
          return this._super(attr);
      }
    }
  });

  exports['default'] = GallerySerializer;

});
define('outdoor-client/serializers/photo', ['exports', 'ember-data'], function (exports, DS) {

  'use strict';

  var PhotoSerializer;

  PhotoSerializer = DS['default'].ActiveModelSerializer.extend(DS['default'].EmbeddedRecordsMixin, {
    attrs: {
      albums: {
        serialize: 'id',
        deserialize: 'ids'
      },
      dataURL: {
        serialize: false
      },
      image_random_thumb: {
        serialize: false
      }
    }
  });

  exports['default'] = PhotoSerializer;

});
define('outdoor-client/serializers/region', ['exports', 'ember-data', 'outdoor-client/mixins/location-serializer'], function (exports, DS, LocationSerializerMixin) {

	'use strict';

	var RegionSerializer;

	RegionSerializer = DS['default'].ActiveModelSerializer.extend(DS['default'].EmbeddedRecordsMixin, LocationSerializerMixin['default'], {});

	exports['default'] = RegionSerializer;

});
define('outdoor-client/serializers/subregion', ['exports', 'ember-data', 'outdoor-client/mixins/location-serializer'], function (exports, DS, LocationSerializerMixin) {

  'use strict';

  var SubregionSerializer;

  SubregionSerializer = DS['default'].ActiveModelSerializer.extend(DS['default'].EmbeddedRecordsMixin, {
    attrs: {
      transportHubs: {
        serialize: 'records',
        deserialize: 'ids'
      },
      contacts: {
        serialize: 'records',
        deserialize: 'ids'
      },
      accommodations: {
        serialize: 'records',
        deserialize: 'ids'
      }
    },
    keyForAttribute: function(attr) {
      switch (attr) {
        case "transportHubs":
          return "transport_hubs_attributes";
        case "contacts":
          return "contacts_attributes";
        case "accommodations":
          return "accommodations_attributes";
        default:
          return this._super(attr);
      }
    }
  });

  exports['default'] = SubregionSerializer;

});
define('outdoor-client/services/current-user', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  var CurrentUserService;

  CurrentUserService = Ember['default'].ObjectProxy.extend({
    content: {},
    init: function() {}
  });

  exports['default'] = CurrentUserService;

});
define('outdoor-client/services/i18n', ['exports', 'ember-cli-i18n/services/i18n'], function (exports, service) {

  'use strict';

  service['default'].getLocalizedPath = function(locale, path, container) {
    var translation;
    translation = container.lookupFactory('i18n:translation').translation;
    return Ember.get(translation[locale], path);
  };

  exports['default'] = service['default'];

});
define('outdoor-client/services/validations', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  var set = Ember['default'].set;

  exports['default'] = Ember['default'].Object.extend({
    init: function() {
      set(this, 'cache', {});
    }
  });

});
define('outdoor-client/templates/application', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, content = hooks.content, inline = hooks.inline;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
          var morph1 = dom.createMorphAt(fragment,1,1,contextualElement);
          dom.insertBoundary(fragment, null);
          dom.insertBoundary(fragment, 0);
          content(env, morph0, context, "modalContent");
          inline(env, morph1, context, "outlet", ["modal"], {});
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","row fullWidth");
        var el2 = dom.createElement("div");
        dom.setAttribute(el2,"class","twelve columns");
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","row fullWidth");
        var el2 = dom.createElement("div");
        dom.setAttribute(el2,"class","twelve columns");
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, inline = hooks.inline, content = hooks.content, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        var morph1 = dom.createMorphAt(dom.childAt(fragment, [1, 0]),0,0);
        var morph2 = dom.createMorphAt(fragment,2,2,contextualElement);
        var morph3 = dom.createMorphAt(dom.childAt(fragment, [3, 0]),0,0);
        var morph4 = dom.createMorphAt(fragment,4,4,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        inline(env, morph0, context, "loading-slider", [], {"isLoading": get(env, context, "loading"), "duration": 400});
        inline(env, morph1, context, "render", ["navbar", get(env, context, "this")], {});
        content(env, morph2, context, "outlet");
        inline(env, morph3, context, "render", ["footer", get(env, context, "this")], {});
        block(env, morph4, context, "reveal-modal", [], {"open": get(env, context, "isOpenModal"), "close": "closeModal", "size": "small"}, child0, null);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/application/locations', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","panel locations");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/components/editable-property', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("a");
          var el2 = dom.createTextNode("Edit");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, content = hooks.content, element = hooks.element;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var element1 = dom.childAt(fragment, [1]);
          var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
          dom.insertBoundary(fragment, 0);
          content(env, morph0, context, "property");
          element(env, element1, context, "action", ["onEdit"], {"on": "click"});
          element(env, element1, context, "bind-attr", [], {"class": ":button :editable-property-btn isHower:show:hide"});
          return fragment;
        }
      };
    }());
    var child1 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","row");
          var el2 = dom.createElement("a");
          dom.setAttribute(el2,"class","button right small");
          var el3 = dom.createTextNode("End EDIT");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, get = hooks.get, inline = hooks.inline, element = hooks.element;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var element0 = dom.childAt(fragment, [1, 0]);
          var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
          dom.insertBoundary(fragment, 0);
          inline(env, morph0, context, "textarea", [], {"value": get(env, context, "propertyTemp"), "rows": 4});
          element(env, element0, context, "action", ["endEdit"], {"on": "click"});
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        block(env, morph0, context, "unless", [get(env, context, "isEdit")], {}, child0, child1);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/components/em-file', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","em-file-field-wrapper");
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("a");
        dom.setAttribute(el2,"class","button");
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, inline = hooks.inline, element = hooks.element, content = hooks.content;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var element0 = dom.childAt(fragment, [0]);
        var element1 = dom.childAt(element0, [1]);
        var morph0 = dom.createMorphAt(element0,0,0);
        var morph1 = dom.createMorphAt(element1,0,0);
        inline(env, morph0, context, "view", [get(env, context, "controlView")], {});
        element(env, element1, context, "action", ["addFile"], {});
        content(env, morph1, context, "yield");
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/components/em-form-control-help', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, content = hooks.content;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        content(env, morph0, context, "helpText");
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/components/em-form-group', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n    ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, get = hooks.get, element = hooks.element, inline = hooks.inline;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var element0 = dom.childAt(fragment, [1]);
          var morph0 = dom.createMorphAt(element0,1,1);
          element(env, element0, context, "bind-attr", [], {"class": get(env, context, "wrapperClass")});
          inline(env, morph0, context, "partial", ["components/formgroup/form-group"], {});
          return fragment;
        }
      };
    }());
    var child1 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, inline = hooks.inline;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(fragment,1,1,contextualElement);
          inline(env, morph0, context, "partial", ["components/formgroup/form-group"], {});
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        block(env, morph0, context, "if", [get(env, context, "wrapperClass")], {}, child0, child1);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/components/em-form-label', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, content = hooks.content;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        var morph1 = dom.createMorphAt(fragment,2,2,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        content(env, morph0, context, "yield");
        content(env, morph1, context, "text");
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/components/em-form-submit', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("button");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n    ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, get = hooks.get, element = hooks.element, content = hooks.content;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var element1 = dom.childAt(fragment, [1]);
          var element2 = dom.childAt(element1, [1]);
          var morph0 = dom.createMorphAt(element2,0,0);
          element(env, element1, context, "bind-attr", [], {"class": get(env, context, "horiClass")});
          element(env, element2, context, "bind-attr", [], {"class": get(env, context, "classes")});
          element(env, element2, context, "bind-attr", [], {"disabled": get(env, context, "disabled")});
          content(env, morph0, context, "text");
          return fragment;
        }
      };
    }());
    var child1 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("button");
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, get = hooks.get, element = hooks.element, content = hooks.content;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var element0 = dom.childAt(fragment, [1]);
          var morph0 = dom.createMorphAt(element0,0,0);
          element(env, element0, context, "bind-attr", [], {"class": get(env, context, "classes")});
          element(env, element0, context, "bind-attr", [], {"disabled": get(env, context, "disabled")});
          content(env, morph0, context, "text");
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        block(env, morph0, context, "if", [get(env, context, "form.isHorizontal")], {}, child0, child1);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/components/em-form', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, content = hooks.content;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(fragment,1,1,contextualElement);
          content(env, morph0, context, "em-form-submit");
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, content = hooks.content, get = hooks.get, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        var morph1 = dom.createMorphAt(fragment,2,2,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        content(env, morph0, context, "yield");
        block(env, morph1, context, "if", [get(env, context, "submit_button")], {}, child0, null);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/components/em-gallery', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      var child0 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createElement("img");
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, get = hooks.get, element = hooks.element;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var element6 = dom.childAt(fragment, [0]);
            element(env, element6, context, "bind-attr", [], {"src": get(env, context, "element.dataURL")});
            element(env, element6, context, "bind-attr", [], {"style": get(env, context, "element.style")});
            return fragment;
          }
        };
      }());
      var child1 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createElement("img");
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, get = hooks.get, element = hooks.element;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var element5 = dom.childAt(fragment, [0]);
            element(env, element5, context, "bind-attr", [], {"src": get(env, context, "element.image_random_thumb")});
            element(env, element5, context, "bind-attr", [], {"style": get(env, context, "element.style")});
            return fragment;
          }
        };
      }());
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("a");
          dom.setAttribute(el1,"class","item");
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, get = hooks.get, element = hooks.element, block = hooks.block;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var element7 = dom.childAt(fragment, [0]);
          var morph0 = dom.createMorphAt(element7,0,0);
          element(env, element7, context, "action", ["showFull", get(env, context, "element")], {});
          element(env, element7, context, "bind-attr", [], {"style": get(env, context, "randomWidth")});
          block(env, morph0, context, "if", [get(env, context, "element.dataURL")], {}, child0, child1);
          return fragment;
        }
      };
    }());
    var child1 = (function() {
      var child0 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createElement("img");
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, get = hooks.get, element = hooks.element;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var element1 = dom.childAt(fragment, [0]);
            element(env, element1, context, "bind-attr", [], {"src": get(env, context, "fullElement.dataURL")});
            element(env, element1, context, "bind-attr", [], {"class": "fullImageClasses"});
            return fragment;
          }
        };
      }());
      var child1 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createElement("img");
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, get = hooks.get, element = hooks.element;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var element0 = dom.childAt(fragment, [0]);
            element(env, element0, context, "bind-attr", [], {"src": get(env, context, "fullElement.image_random_thumb")});
            element(env, element0, context, "bind-attr", [], {"class": "fullImageClasses"});
            return fragment;
          }
        };
      }());
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","em-gallery-overlay");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("a");
          dom.setAttribute(el1,"class","close");
          var el2 = dom.createTextNode("X");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("a");
          dom.setAttribute(el1,"class","prevNext prev");
          var el2 = dom.createElement("p");
          var el3 = dom.createElement("i");
          dom.setAttribute(el3,"class","fi-previous");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("a");
          dom.setAttribute(el1,"class","prevNext next");
          var el2 = dom.createElement("p");
          var el3 = dom.createElement("i");
          dom.setAttribute(el3,"class","fi-next");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, element = hooks.element, get = hooks.get, block = hooks.block;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var element2 = dom.childAt(fragment, [1]);
          var element3 = dom.childAt(fragment, [2]);
          var element4 = dom.childAt(fragment, [3]);
          var morph0 = dom.createMorphAt(fragment,4,4,contextualElement);
          dom.insertBoundary(fragment, null);
          element(env, element2, context, "action", ["close"], {});
          element(env, element3, context, "action", ["toPrev"], {});
          element(env, element4, context, "action", ["toNext"], {});
          block(env, morph0, context, "if", [get(env, context, "fullElement.dataURL")], {}, child0, child1);
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","grid-sizer");
        dom.appendChild(el0, el1);
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,1,1,contextualElement);
        var morph1 = dom.createMorphAt(fragment,2,2,contextualElement);
        dom.insertBoundary(fragment, null);
        block(env, morph0, context, "each", [get(env, context, "elements")], {"keyword": "element"}, child0, null);
        block(env, morph1, context, "if", [get(env, context, "fullElement")], {}, child1, null);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/components/formgroup/control-within-label', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, inline = hooks.inline;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(fragment,1,1,contextualElement);
          inline(env, morph0, context, "partial", ["components/formgroup/form-group-control"], {});
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        block(env, morph0, context, "em-form-label", [], {"text": get(env, context, "label"), "horiClass": "", "inlineClass": "", "viewName": get(env, context, "labelViewName")}, child0, null);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/components/formgroup/form-group-control', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n    ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, get = hooks.get, element = hooks.element, inline = hooks.inline;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var element0 = dom.childAt(fragment, [1]);
          var morph0 = dom.createMorphAt(element0,1,1);
          element(env, element0, context, "bind-attr", [], {"class": get(env, context, "controlWrapper")});
          inline(env, morph0, context, "view", [get(env, context, "controlView")], {"viewName": get(env, context, "controlViewName"), "property": get(env, context, "propertyName"), "id": get(env, context, "cid")});
          return fragment;
        }
      };
    }());
    var child1 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, get = hooks.get, inline = hooks.inline;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(fragment,1,1,contextualElement);
          inline(env, morph0, context, "view", [get(env, context, "controlView")], {"viewName": get(env, context, "controlViewName"), "property": get(env, context, "propertyName"), "id": get(env, context, "cid")});
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        block(env, morph0, context, "if", [get(env, context, "controlWrapper")], {}, child0, child1);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/components/formgroup/form-group', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      var child0 = (function() {
        var child0 = (function() {
          var child0 = (function() {
            return {
              isHTMLBars: true,
              revision: "Ember@1.11.0",
              blockParams: 0,
              cachedFragment: null,
              hasRendered: false,
              build: function build(dom) {
                var el0 = dom.createDocumentFragment();
                var el1 = dom.createTextNode("                ");
                dom.appendChild(el0, el1);
                var el1 = dom.createElement("div");
                var el2 = dom.createTextNode("\n                    ");
                dom.appendChild(el1, el2);
                var el2 = dom.createComment("");
                dom.appendChild(el1, el2);
                var el2 = dom.createTextNode("\n                ");
                dom.appendChild(el1, el2);
                dom.appendChild(el0, el1);
                var el1 = dom.createTextNode("\n");
                dom.appendChild(el0, el1);
                return el0;
              },
              render: function render(context, env, contextualElement) {
                var dom = env.dom;
                var hooks = env.hooks, get = hooks.get, element = hooks.element, inline = hooks.inline;
                dom.detectNamespace(contextualElement);
                var fragment;
                if (env.useFragmentCache && dom.canClone) {
                  if (this.cachedFragment === null) {
                    fragment = this.build(dom);
                    if (this.hasRendered) {
                      this.cachedFragment = fragment;
                    } else {
                      this.hasRendered = true;
                    }
                  }
                  if (this.cachedFragment) {
                    fragment = dom.cloneNode(this.cachedFragment, true);
                  }
                } else {
                  fragment = this.build(dom);
                }
                var element2 = dom.childAt(fragment, [1]);
                var morph0 = dom.createMorphAt(element2,1,1);
                element(env, element2, context, "bind-attr", [], {"class": get(env, context, "labelWrapperClass")});
                inline(env, morph0, context, "partial", ["components/formgroup/control-within-label"], {});
                return fragment;
              }
            };
          }());
          var child1 = (function() {
            return {
              isHTMLBars: true,
              revision: "Ember@1.11.0",
              blockParams: 0,
              cachedFragment: null,
              hasRendered: false,
              build: function build(dom) {
                var el0 = dom.createDocumentFragment();
                var el1 = dom.createTextNode("                ");
                dom.appendChild(el0, el1);
                var el1 = dom.createComment("");
                dom.appendChild(el0, el1);
                var el1 = dom.createTextNode("\n");
                dom.appendChild(el0, el1);
                return el0;
              },
              render: function render(context, env, contextualElement) {
                var dom = env.dom;
                var hooks = env.hooks, inline = hooks.inline;
                dom.detectNamespace(contextualElement);
                var fragment;
                if (env.useFragmentCache && dom.canClone) {
                  if (this.cachedFragment === null) {
                    fragment = this.build(dom);
                    if (this.hasRendered) {
                      this.cachedFragment = fragment;
                    } else {
                      this.hasRendered = true;
                    }
                  }
                  if (this.cachedFragment) {
                    fragment = dom.cloneNode(this.cachedFragment, true);
                  }
                } else {
                  fragment = this.build(dom);
                }
                var morph0 = dom.createMorphAt(fragment,1,1,contextualElement);
                inline(env, morph0, context, "partial", ["components/formgroup/control-within-label"], {});
                return fragment;
              }
            };
          }());
          return {
            isHTMLBars: true,
            revision: "Ember@1.11.0",
            blockParams: 0,
            cachedFragment: null,
            hasRendered: false,
            build: function build(dom) {
              var el0 = dom.createDocumentFragment();
              var el1 = dom.createComment("");
              dom.appendChild(el0, el1);
              return el0;
            },
            render: function render(context, env, contextualElement) {
              var dom = env.dom;
              var hooks = env.hooks, get = hooks.get, block = hooks.block;
              dom.detectNamespace(contextualElement);
              var fragment;
              if (env.useFragmentCache && dom.canClone) {
                if (this.cachedFragment === null) {
                  fragment = this.build(dom);
                  if (this.hasRendered) {
                    this.cachedFragment = fragment;
                  } else {
                    this.hasRendered = true;
                  }
                }
                if (this.cachedFragment) {
                  fragment = dom.cloneNode(this.cachedFragment, true);
                }
              } else {
                fragment = this.build(dom);
              }
              var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
              dom.insertBoundary(fragment, null);
              dom.insertBoundary(fragment, 0);
              block(env, morph0, context, "if", [get(env, context, "labelWrapperClass")], {}, child0, child1);
              return fragment;
            }
          };
        }());
        var child1 = (function() {
          var child0 = (function() {
            return {
              isHTMLBars: true,
              revision: "Ember@1.11.0",
              blockParams: 0,
              cachedFragment: null,
              hasRendered: false,
              build: function build(dom) {
                var el0 = dom.createDocumentFragment();
                var el1 = dom.createTextNode("                ");
                dom.appendChild(el0, el1);
                var el1 = dom.createElement("div");
                var el2 = dom.createTextNode("\n                    ");
                dom.appendChild(el1, el2);
                var el2 = dom.createComment("");
                dom.appendChild(el1, el2);
                var el2 = dom.createTextNode("\n                    ");
                dom.appendChild(el1, el2);
                var el2 = dom.createComment("");
                dom.appendChild(el1, el2);
                var el2 = dom.createTextNode("\n                ");
                dom.appendChild(el1, el2);
                dom.appendChild(el0, el1);
                var el1 = dom.createTextNode("\n");
                dom.appendChild(el0, el1);
                return el0;
              },
              render: function render(context, env, contextualElement) {
                var dom = env.dom;
                var hooks = env.hooks, get = hooks.get, element = hooks.element, inline = hooks.inline;
                dom.detectNamespace(contextualElement);
                var fragment;
                if (env.useFragmentCache && dom.canClone) {
                  if (this.cachedFragment === null) {
                    fragment = this.build(dom);
                    if (this.hasRendered) {
                      this.cachedFragment = fragment;
                    } else {
                      this.hasRendered = true;
                    }
                  }
                  if (this.cachedFragment) {
                    fragment = dom.cloneNode(this.cachedFragment, true);
                  }
                } else {
                  fragment = this.build(dom);
                }
                var element1 = dom.childAt(fragment, [1]);
                var morph0 = dom.createMorphAt(element1,1,1);
                var morph1 = dom.createMorphAt(element1,3,3);
                element(env, element1, context, "bind-attr", [], {"class": get(env, context, "labelWrapperClass")});
                inline(env, morph0, context, "em-form-label", [], {"text": get(env, context, "label"), "viewName": get(env, context, "labelViewName")});
                inline(env, morph1, context, "partial", ["components/formgroup/form-group-control"], {});
                return fragment;
              }
            };
          }());
          var child1 = (function() {
            return {
              isHTMLBars: true,
              revision: "Ember@1.11.0",
              blockParams: 0,
              cachedFragment: null,
              hasRendered: false,
              build: function build(dom) {
                var el0 = dom.createDocumentFragment();
                var el1 = dom.createTextNode("                ");
                dom.appendChild(el0, el1);
                var el1 = dom.createComment("");
                dom.appendChild(el0, el1);
                var el1 = dom.createTextNode("\n                ");
                dom.appendChild(el0, el1);
                var el1 = dom.createComment("");
                dom.appendChild(el0, el1);
                var el1 = dom.createTextNode("\n");
                dom.appendChild(el0, el1);
                return el0;
              },
              render: function render(context, env, contextualElement) {
                var dom = env.dom;
                var hooks = env.hooks, get = hooks.get, inline = hooks.inline;
                dom.detectNamespace(contextualElement);
                var fragment;
                if (env.useFragmentCache && dom.canClone) {
                  if (this.cachedFragment === null) {
                    fragment = this.build(dom);
                    if (this.hasRendered) {
                      this.cachedFragment = fragment;
                    } else {
                      this.hasRendered = true;
                    }
                  }
                  if (this.cachedFragment) {
                    fragment = dom.cloneNode(this.cachedFragment, true);
                  }
                } else {
                  fragment = this.build(dom);
                }
                var morph0 = dom.createMorphAt(fragment,1,1,contextualElement);
                var morph1 = dom.createMorphAt(fragment,3,3,contextualElement);
                inline(env, morph0, context, "em-form-label", [], {"text": get(env, context, "label"), "viewName": get(env, context, "labelViewName")});
                inline(env, morph1, context, "partial", ["components/formgroup/form-group-control"], {});
                return fragment;
              }
            };
          }());
          return {
            isHTMLBars: true,
            revision: "Ember@1.11.0",
            blockParams: 0,
            cachedFragment: null,
            hasRendered: false,
            build: function build(dom) {
              var el0 = dom.createDocumentFragment();
              var el1 = dom.createComment("");
              dom.appendChild(el0, el1);
              return el0;
            },
            render: function render(context, env, contextualElement) {
              var dom = env.dom;
              var hooks = env.hooks, get = hooks.get, block = hooks.block;
              dom.detectNamespace(contextualElement);
              var fragment;
              if (env.useFragmentCache && dom.canClone) {
                if (this.cachedFragment === null) {
                  fragment = this.build(dom);
                  if (this.hasRendered) {
                    this.cachedFragment = fragment;
                  } else {
                    this.hasRendered = true;
                  }
                }
                if (this.cachedFragment) {
                  fragment = dom.cloneNode(this.cachedFragment, true);
                }
              } else {
                fragment = this.build(dom);
              }
              var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
              dom.insertBoundary(fragment, null);
              dom.insertBoundary(fragment, 0);
              block(env, morph0, context, "if", [get(env, context, "labelWrapperClass")], {}, child0, child1);
              return fragment;
            }
          };
        }());
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createComment("");
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, get = hooks.get, block = hooks.block;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
            dom.insertBoundary(fragment, null);
            dom.insertBoundary(fragment, 0);
            block(env, morph0, context, "if", [get(env, context, "yieldInLabel")], {}, child0, child1);
            return fragment;
          }
        };
      }());
      var child1 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("        ");
            dom.appendChild(el0, el1);
            var el1 = dom.createComment("");
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, inline = hooks.inline;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var morph0 = dom.createMorphAt(fragment,1,1,contextualElement);
            inline(env, morph0, context, "partial", ["components/formgroup/form-group-control"], {});
            return fragment;
          }
        };
      }());
      var child2 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("        ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("span");
            dom.setAttribute(el1,"class","form-control-feedback");
            var el2 = dom.createElement("i");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, get = hooks.get, element = hooks.element;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var element0 = dom.childAt(fragment, [1, 0]);
            element(env, element0, context, "bind-attr", [], {"class": get(env, context, "v_icon")});
            return fragment;
          }
        };
      }());
      var child3 = (function() {
        var child0 = (function() {
          return {
            isHTMLBars: true,
            revision: "Ember@1.11.0",
            blockParams: 0,
            cachedFragment: null,
            hasRendered: false,
            build: function build(dom) {
              var el0 = dom.createDocumentFragment();
              var el1 = dom.createTextNode("            ");
              dom.appendChild(el0, el1);
              var el1 = dom.createComment("");
              dom.appendChild(el0, el1);
              var el1 = dom.createTextNode("\n");
              dom.appendChild(el0, el1);
              return el0;
            },
            render: function render(context, env, contextualElement) {
              var dom = env.dom;
              var hooks = env.hooks, get = hooks.get, inline = hooks.inline;
              dom.detectNamespace(contextualElement);
              var fragment;
              if (env.useFragmentCache && dom.canClone) {
                if (this.cachedFragment === null) {
                  fragment = this.build(dom);
                  if (this.hasRendered) {
                    this.cachedFragment = fragment;
                  } else {
                    this.hasRendered = true;
                  }
                }
                if (this.cachedFragment) {
                  fragment = dom.cloneNode(this.cachedFragment, true);
                }
              } else {
                fragment = this.build(dom);
              }
              var morph0 = dom.createMorphAt(fragment,1,1,contextualElement);
              inline(env, morph0, context, "em-form-control-help", [], {"text": get(env, context, "help"), "viewName": get(env, context, "helpViewName")});
              return fragment;
            }
          };
        }());
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createComment("");
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, get = hooks.get, block = hooks.block;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
            dom.insertBoundary(fragment, null);
            dom.insertBoundary(fragment, 0);
            block(env, morph0, context, "if", [get(env, context, "canShowErrors")], {}, child0, null);
            return fragment;
          }
        };
      }());
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, get = hooks.get, block = hooks.block;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
          var morph1 = dom.createMorphAt(fragment,2,2,contextualElement);
          var morph2 = dom.createMorphAt(fragment,4,4,contextualElement);
          dom.insertBoundary(fragment, null);
          dom.insertBoundary(fragment, 0);
          block(env, morph0, context, "if", [get(env, context, "label")], {}, child0, child1);
          block(env, morph1, context, "if", [get(env, context, "v_icons")], {}, child2, null);
          block(env, morph2, context, "unless", [get(env, context, "form.isInline")], {}, child3, null);
          return fragment;
        }
      };
    }());
    var child1 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, content = hooks.content;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(fragment,1,1,contextualElement);
          content(env, morph0, context, "yield");
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        block(env, morph0, context, "unless", [get(env, context, "template")], {}, child0, child1);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/components/gm-location-autocomplite', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, inline = hooks.inline;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        inline(env, morph0, context, "view", [get(env, context, "selectView")], {});
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/components/gm-map', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"style","min-height: 420px");
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, inline = hooks.inline, content = hooks.content;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var element0 = dom.childAt(fragment, [0]);
        var morph0 = dom.createMorphAt(element0,0,0);
        var morph1 = dom.createMorphAt(element0,1,1);
        inline(env, morph0, context, "view", [get(env, context, "mapCanvas")], {});
        content(env, morph1, context, "yield");
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/components/gm-select', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, inline = hooks.inline;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        inline(env, morph0, context, "view", [get(env, context, "selectView")], {});
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/components/masonry-grid', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, content = hooks.content;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        dom.insertBoundary(fragment, 0);
        content(env, morph0, context, "yield");
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/components/simple-select', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, inline = hooks.inline;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        inline(env, morph0, context, "view", [get(env, context, "selectView")], {});
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/confirm', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("h3");
        var el2 = dom.createTextNode("Are you sure?");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("a");
        dom.setAttribute(el1,"class","button alert");
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("a");
        dom.setAttribute(el1,"class","button success");
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, element = hooks.element, inline = hooks.inline, get = hooks.get;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var element0 = dom.childAt(fragment, [1]);
        var element1 = dom.childAt(fragment, [2]);
        var morph0 = dom.createMorphAt(element0,0,0);
        var morph1 = dom.createMorphAt(element1,0,0);
        element(env, element0, context, "action", ["cancelConfirm"], {"on": "click"});
        inline(env, morph0, context, "t", ["cancel"], {});
        element(env, element1, context, "action", ["confirmed", get(env, context, "confirm")], {});
        inline(env, morph1, context, "t", ["yes"], {});
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/footer', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","panel footer");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/index', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","row");
          var el2 = dom.createElement("div");
          dom.setAttribute(el2,"class","large-2 columns");
          var el3 = dom.createElement("span");
          var el4 = dom.createElement("b");
          var el5 = dom.createTextNode("+ ");
          dom.appendChild(el4, el5);
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("div");
          dom.setAttribute(el2,"style","margin-top: 12px;");
          dom.setAttribute(el2,"class","large-10 columns");
          var el3 = dom.createElement("b");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, inline = hooks.inline;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(dom.childAt(fragment, [0, 1, 0]),0,0);
          inline(env, morph0, context, "t", ["middle_nav.add_new_route"], {});
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","row fullWidth");
        var el2 = dom.createElement("div");
        dom.setAttribute(el2,"class","twelve columns");
        var el3 = dom.createElement("img");
        dom.setAttribute(el3,"src","map-9a9cd3405d7bf6834988fed8f9a3b86a.jpg");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2,"class","twelve columns");
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2,"class","twelve columns text-center");
        var el3 = dom.createElement("div");
        dom.setAttribute(el3,"class","panel banner");
        var el4 = dom.createElement("img");
        dom.setAttribute(el4,"src","horizontal_banner-e33574b60e4e8bd5cd2cc10e4b07f46d.jpg");
        dom.setAttribute(el4,"class","vcenter");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2,"class","twelve columns");
        var el3 = dom.createElement("dl");
        dom.setAttribute(el3,"class","routes-sub-nav");
        var el4 = dom.createElement("div");
        dom.setAttribute(el4,"class","vcenter");
        var el5 = dom.createElement("dd");
        dom.setAttribute(el5,"class","active");
        var el6 = dom.createElement("a");
        dom.setAttribute(el6,"href","#");
        var el7 = dom.createComment("");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("dd");
        var el6 = dom.createElement("a");
        dom.setAttribute(el6,"href","#");
        var el7 = dom.createComment("");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("dd");
        var el6 = dom.createElement("a");
        dom.setAttribute(el6,"href","#");
        var el7 = dom.createComment("");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("dd");
        var el6 = dom.createElement("a");
        dom.setAttribute(el6,"href","#");
        var el7 = dom.createComment("");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, inline = hooks.inline, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var element0 = dom.childAt(fragment, [0]);
        var element1 = dom.childAt(element0, [3, 0]);
        var element2 = dom.childAt(element1, [0]);
        var morph0 = dom.createMorphAt(dom.childAt(element0, [1]),0,0);
        var morph1 = dom.createMorphAt(dom.childAt(element2, [0, 0]),0,0);
        var morph2 = dom.createMorphAt(dom.childAt(element2, [1, 0]),0,0);
        var morph3 = dom.createMorphAt(dom.childAt(element2, [2, 0]),0,0);
        var morph4 = dom.createMorphAt(dom.childAt(element2, [3, 0]),0,0);
        var morph5 = dom.createMorphAt(element1,1,1);
        inline(env, morph0, context, "render", ["application/locations"], {});
        inline(env, morph1, context, "t", ["middle_nav.featured_routs"], {});
        inline(env, morph2, context, "t", ["middle_nav.new_routs"], {});
        inline(env, morph3, context, "t", ["middle_nav.popular_routs"], {});
        inline(env, morph4, context, "t", ["middle_nav.reports"], {});
        block(env, morph5, context, "link-to", ["routes.new"], {"class": "button right"}, child0, null);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/login', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","form-actions");
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, subexpr = hooks.subexpr, inline = hooks.inline, get = hooks.get;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
          var morph1 = dom.createMorphAt(fragment,1,1,contextualElement);
          var morph2 = dom.createMorphAt(dom.childAt(fragment, [2]),0,0);
          dom.insertBoundary(fragment, 0);
          inline(env, morph0, context, "em-input", [], {"property": "identification", "label": subexpr(env, context, "t", ["auth_forms.email"], {}), "placeholder": subexpr(env, context, "t", ["auth_forms.email_ph"], {}), "type": "email"});
          inline(env, morph1, context, "em-input", [], {"property": "password", "label": subexpr(env, context, "t", ["auth_forms.password"], {}), "placeholder": subexpr(env, context, "t", ["auth_forms.password_ph"], {}), "type": "password", "disabled": get(env, context, "identificationHasValue")});
          inline(env, morph2, context, "input", [], {"class": get(env, context, "submitClasses"), "type": "submit", "value": subexpr(env, context, "t", ["auth_forms.login_btn"], {})});
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        block(env, morph0, context, "em-form", [], {"model": get(env, context, "controller"), "action": "authenticate", "submit_button": false}, child0, null);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/navbar', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("img");
          dom.setAttribute(el1,"src","brand-c3155fe1746e8db38ba139be58a5e39c.png");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          return fragment;
        }
      };
    }());
    var child1 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("strong");
          var el2 = dom.createTextNode("OUTDOOR.ROCKS!");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          return fragment;
        }
      };
    }());
    var child2 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("strong");
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, inline = hooks.inline;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(dom.childAt(fragment, [0]),0,0);
          inline(env, morph0, context, "t", ["navbar.home"], {});
          return fragment;
        }
      };
    }());
    var child3 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("strong");
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, inline = hooks.inline;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(dom.childAt(fragment, [0]),0,0);
          inline(env, morph0, context, "t", ["navbar.about"], {});
          return fragment;
        }
      };
    }());
    var child4 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("strong");
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, inline = hooks.inline;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(dom.childAt(fragment, [0]),0,0);
          inline(env, morph0, context, "t", ["navbar.projects"], {});
          return fragment;
        }
      };
    }());
    var child5 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("strong");
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, inline = hooks.inline;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(dom.childAt(fragment, [0]),0,0);
          inline(env, morph0, context, "t", ["navbar.partners"], {});
          return fragment;
        }
      };
    }());
    var child6 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("strong");
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, inline = hooks.inline;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(dom.childAt(fragment, [0]),0,0);
          inline(env, morph0, context, "t", ["navbar.blog"], {});
          return fragment;
        }
      };
    }());
    var child7 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("strong");
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, inline = hooks.inline;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(dom.childAt(fragment, [0]),0,0);
          inline(env, morph0, context, "t", ["navbar.feedback"], {});
          return fragment;
        }
      };
    }());
    var child8 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("img");
          dom.setAttribute(el1,"src","icon_search-fee7e4fcb87c23b9cda389f5e3f66488.png");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          return fragment;
        }
      };
    }());
    var child9 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("a");
          dom.setAttribute(el1,"class","button");
          var el2 = dom.createElement("strong");
          var el3 = dom.createElement("pre");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, element = hooks.element, inline = hooks.inline;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var element1 = dom.childAt(fragment, [0]);
          var morph0 = dom.createMorphAt(dom.childAt(element1, [0, 0]),0,0);
          element(env, element1, context, "action", ["invalidateSession"], {"on": "click"});
          inline(env, morph0, context, "t", ["navbar.logout"], {});
          return fragment;
        }
      };
    }());
    var child10 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("a");
          dom.setAttribute(el1,"class","button");
          var el2 = dom.createElement("strong");
          var el3 = dom.createElement("pre");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, element = hooks.element, inline = hooks.inline;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var element0 = dom.childAt(fragment, [0]);
          var morph0 = dom.createMorphAt(dom.childAt(element0, [0, 0]),0,0);
          element(env, element0, context, "action", ["login"], {"on": "click"});
          inline(env, morph0, context, "t", ["navbar.login"], {});
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("ul");
        dom.setAttribute(el1,"class","title-area");
        var el2 = dom.createElement("li");
        dom.setAttribute(el2,"class","name");
        var el3 = dom.createElement("h1");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("li");
        dom.setAttribute(el2,"class","toggle-topbar menu-icon");
        var el3 = dom.createElement("a");
        dom.setAttribute(el3,"href","#");
        var el4 = dom.createElement("span");
        var el5 = dom.createTextNode("Menu");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","top-bar-section");
        var el2 = dom.createElement("ul");
        dom.setAttribute(el2,"class","right");
        var el3 = dom.createElement("li");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("li");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("li");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("li");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("li");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("li");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("li");
        dom.setAttribute(el3,"class","divider");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("li");
        dom.setAttribute(el3,"class","has-form");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("li");
        dom.setAttribute(el3,"class","divider");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("li");
        dom.setAttribute(el3,"class","has-dropdown");
        var el4 = dom.createElement("a");
        dom.setAttribute(el4,"href","#");
        var el5 = dom.createElement("strong");
        var el6 = dom.createComment("");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("ul");
        dom.setAttribute(el4,"style","width: 100px");
        dom.setAttribute(el4,"class","dropdown");
        var el5 = dom.createElement("li");
        dom.setAttribute(el5,"id","en");
        dom.setAttribute(el5,"class","active");
        var el6 = dom.createElement("a");
        var el7 = dom.createElement("img");
        dom.setAttribute(el7,"src","countries-geojson/gbr.svg");
        dom.setAttribute(el7,"style","width: 80px");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("li");
        dom.setAttribute(el5,"id","ru");
        var el6 = dom.createElement("a");
        var el7 = dom.createElement("img");
        dom.setAttribute(el7,"src","countries-geojson/rus.svg");
        dom.setAttribute(el7,"style","width: 80px");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("li");
        dom.setAttribute(el3,"class","divider");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("li");
        dom.setAttribute(el3,"class","has-form");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, block = hooks.block, get = hooks.get, inline = hooks.inline, element = hooks.element;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var element2 = dom.childAt(fragment, [0, 0, 0]);
        var element3 = dom.childAt(fragment, [1, 0]);
        var element4 = dom.childAt(element3, [9]);
        var element5 = dom.childAt(element4, [1]);
        var element6 = dom.childAt(element5, [0, 0]);
        var element7 = dom.childAt(element5, [1, 0]);
        var morph0 = dom.createMorphAt(element2,0,0);
        var morph1 = dom.createMorphAt(element2,1,1);
        var morph2 = dom.createMorphAt(dom.childAt(element3, [0]),0,0);
        var morph3 = dom.createMorphAt(dom.childAt(element3, [1]),0,0);
        var morph4 = dom.createMorphAt(dom.childAt(element3, [2]),0,0);
        var morph5 = dom.createMorphAt(dom.childAt(element3, [3]),0,0);
        var morph6 = dom.createMorphAt(dom.childAt(element3, [4]),0,0);
        var morph7 = dom.createMorphAt(dom.childAt(element3, [5]),0,0);
        var morph8 = dom.createMorphAt(dom.childAt(element3, [7]),0,0);
        var morph9 = dom.createMorphAt(dom.childAt(element4, [0, 0]),0,0);
        var morph10 = dom.createMorphAt(dom.childAt(element3, [11]),0,0);
        block(env, morph0, context, "link-to", ["application"], {}, child0, null);
        block(env, morph1, context, "link-to", ["application"], {}, child1, null);
        block(env, morph2, context, "link-to", ["index"], {}, child2, null);
        block(env, morph3, context, "link-to", ["application"], {}, child3, null);
        block(env, morph4, context, "link-to", ["application"], {}, child4, null);
        block(env, morph5, context, "link-to", ["application"], {}, child5, null);
        block(env, morph6, context, "link-to", ["application"], {}, child6, null);
        block(env, morph7, context, "link-to", ["application"], {}, child7, null);
        block(env, morph8, context, "link-to", ["application"], {"class": "vcenter"}, child8, null);
        inline(env, morph9, context, "em-capitalize", [get(env, context, "model.lang")], {});
        element(env, element6, context, "action", ["changeLang", "en"], {});
        element(env, element7, context, "action", ["changeLang", "ru"], {});
        block(env, morph10, context, "if", [get(env, context, "session.isAuthenticated")], {}, child9, child10);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/reveal-modal', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("a");
        dom.setAttribute(el1,"class","close-reveal-modal");
        var el2 = dom.createTextNode("x");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, content = hooks.content;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        dom.insertBoundary(fragment, 0);
        content(env, morph0, context, "yield");
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/routes', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, content = hooks.content;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        content(env, morph0, context, "outlet");
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/routes/new', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      var child0 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createElement("li");
            var el2 = dom.createComment("");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, content = hooks.content;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var morph0 = dom.createMorphAt(dom.childAt(fragment, [0]),0,0);
            content(env, morph0, context, "region.name");
            return fragment;
          }
        };
      }());
      var child1 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createElement("li");
            var el2 = dom.createComment("");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, content = hooks.content;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var morph0 = dom.createMorphAt(dom.childAt(fragment, [0]),0,0);
            content(env, morph0, context, "subregion.name");
            return fragment;
          }
        };
      }());
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("h2");
          var el2 = dom.createElement("ul");
          dom.setAttribute(el2,"class","breadcrumbs");
          var el3 = dom.createElement("li");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, content = hooks.content, get = hooks.get, block = hooks.block;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var element0 = dom.childAt(fragment, [0, 0]);
          var morph0 = dom.createMorphAt(dom.childAt(element0, [0]),0,0);
          var morph1 = dom.createMorphAt(element0,1,1);
          var morph2 = dom.createMorphAt(element0,2,2);
          content(env, morph0, context, "country.name");
          block(env, morph1, context, "if", [get(env, context, "region")], {}, child0, null);
          block(env, morph2, context, "if", [get(env, context, "subregion")], {}, child1, null);
          return fragment;
        }
      };
    }());
    var child1 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, content = hooks.content;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
          dom.insertBoundary(fragment, null);
          dom.insertBoundary(fragment, 0);
          content(env, morph0, context, "outlet");
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","row fullWidth");
        var el2 = dom.createElement("div");
        dom.setAttribute(el2,"class","twelve columns");
        var el3 = dom.createElement("ul");
        dom.setAttribute(el3,"class","routes-wizard text-right");
        var el4 = dom.createElement("li");
        var el5 = dom.createElement("a");
        dom.setAttribute(el5,"class","left");
        var el6 = dom.createComment("");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("div");
        dom.setAttribute(el5,"class","chevron right");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("li");
        var el5 = dom.createElement("a");
        dom.setAttribute(el5,"class","left");
        var el6 = dom.createComment("");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("div");
        dom.setAttribute(el5,"class","chevron right");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("li");
        var el5 = dom.createElement("a");
        dom.setAttribute(el5,"class","left");
        var el6 = dom.createComment("");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("div");
        dom.setAttribute(el5,"class","chevron right");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2,"class","twelve columns");
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3,"class","row collapse");
        var el4 = dom.createElement("div");
        dom.setAttribute(el4,"class","large-12 columns");
        var el5 = dom.createComment("");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, element = hooks.element, inline = hooks.inline, get = hooks.get, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var element1 = dom.childAt(fragment, [0]);
        var element2 = dom.childAt(element1, [0, 0]);
        var element3 = dom.childAt(element2, [0]);
        var element4 = dom.childAt(element3, [0]);
        var element5 = dom.childAt(element2, [1]);
        var element6 = dom.childAt(element5, [0]);
        var element7 = dom.childAt(element2, [2]);
        var element8 = dom.childAt(element7, [0]);
        var element9 = dom.childAt(element1, [1]);
        var morph0 = dom.createMorphAt(element4,0,0);
        var morph1 = dom.createMorphAt(element6,0,0);
        var morph2 = dom.createMorphAt(element8,0,0);
        var morph3 = dom.createMorphAt(element9,0,0);
        var morph4 = dom.createMorphAt(dom.childAt(element9, [1, 0]),0,0);
        element(env, element3, context, "bind-attr", [], {"class": "countryState isCountryChoosed:enabled:disabled"});
        element(env, element4, context, "action", ["onCountry"], {"on": "click"});
        inline(env, morph0, context, "t", ["routes.new.country"], {});
        element(env, element5, context, "bind-attr", [], {"class": "regionState isRegionChoosed:enabled:disabled"});
        element(env, element6, context, "action", ["onRegion"], {"on": "click"});
        inline(env, morph1, context, "t", ["routes.new.region"], {});
        element(env, element7, context, "bind-attr", [], {"class": "subregionState isSubregionChoosed:enabled:disabled"});
        element(env, element8, context, "action", ["onSubregion"], {"on": "click"});
        inline(env, morph2, context, "t", ["routes.new.subregion"], {});
        block(env, morph3, context, "if", [get(env, context, "country")], {}, child0, null);
        block(env, morph4, context, "gm-map", [], {}, child1, null);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/routes/new/country', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("hr");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, inline = hooks.inline;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(fragment,1,1,contextualElement);
          var morph1 = dom.createMorphAt(fragment,2,2,contextualElement);
          var morph2 = dom.createMorphAt(fragment,3,3,contextualElement);
          var morph3 = dom.createMorphAt(fragment,4,4,contextualElement);
          var morph4 = dom.createMorphAt(fragment,5,5,contextualElement);
          var morph5 = dom.createMorphAt(fragment,6,6,contextualElement);
          dom.insertBoundary(fragment, null);
          inline(env, morph0, context, "partial", ["routes/new/partials/choose-location-name"], {});
          inline(env, morph1, context, "partial", ["routes/new/partials/choose-location-visa"], {});
          inline(env, morph2, context, "partial", ["routes/new/partials/choose-location-description"], {});
          inline(env, morph3, context, "partial", ["routes/new/partials/choose-location-transport-info"], {});
          inline(env, morph4, context, "partial", ["routes/new/partials/choose-location-transport-hubs"], {});
          inline(env, morph5, context, "partial", ["routes/new/partials/choose-location-contacts"], {});
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, inline = hooks.inline, get = hooks.get, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        var morph1 = dom.createMorphAt(fragment,1,1,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        inline(env, morph0, context, "partial", ["routes/new/partials/choose-location-head"], {});
        block(env, morph1, context, "if", [get(env, context, "isLocationChoosed")], {}, child0, null);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/routes/new/country/create', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      var child0 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            return fragment;
          }
        };
      }());
      var child1 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createComment("");
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, inline = hooks.inline;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
            dom.insertBoundary(fragment, null);
            dom.insertBoundary(fragment, 0);
            inline(env, morph0, context, "t", ["cancel"], {});
            return fragment;
          }
        };
      }());
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","row collapse");
          var el2 = dom.createElement("div");
          dom.setAttribute(el2,"class","large-10");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("div");
          dom.setAttribute(el2,"class","large-3");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("div");
          dom.setAttribute(el2,"class","large-10");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("label");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("a");
          dom.setAttribute(el3,"class","button small");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("label");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("a");
          dom.setAttribute(el3,"class","button small");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("label");
          var el4 = dom.createTextNode("Contry GALLERY");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("a");
          dom.setAttribute(el3,"class","button small");
          var el4 = dom.createTextNode("Add PHOTO to GALLERY");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","row collapse");
          var el2 = dom.createElement("div");
          dom.setAttribute(el2,"class","large-6 offset-3");
          var el3 = dom.createElement("div");
          dom.setAttribute(el3,"class","form-actions");
          var el4 = dom.createElement("br");
          dom.appendChild(el3, el4);
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          var el4 = dom.createElement("input");
          dom.setAttribute(el4,"type","submit");
          dom.setAttribute(el4,"value","Save");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, subexpr = hooks.subexpr, inline = hooks.inline, element = hooks.element, get = hooks.get, block = hooks.block;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var element0 = dom.childAt(fragment, [0]);
          var element1 = dom.childAt(element0, [2]);
          var element2 = dom.childAt(element1, [4]);
          var element3 = dom.childAt(element1, [7]);
          var element4 = dom.childAt(element1, [9]);
          var element5 = dom.childAt(fragment, [2, 0, 0]);
          var element6 = dom.childAt(element5, [2]);
          var morph0 = dom.createMorphAt(dom.childAt(element0, [0]),0,0);
          var morph1 = dom.createMorphAt(dom.childAt(element0, [1]),0,0);
          var morph2 = dom.createMorphAt(element1,0,0);
          var morph3 = dom.createMorphAt(element1,1,1);
          var morph4 = dom.createMorphAt(dom.childAt(element1, [2]),0,0);
          var morph5 = dom.createMorphAt(element1,3,3);
          var morph6 = dom.createMorphAt(element2,0,0);
          var morph7 = dom.createMorphAt(dom.childAt(element1, [5]),0,0);
          var morph8 = dom.createMorphAt(element1,6,6);
          var morph9 = dom.createMorphAt(element3,0,0);
          var morph10 = dom.createMorphAt(fragment,1,1,contextualElement);
          var morph11 = dom.createMorphAt(element5,1,1);
          inline(env, morph0, context, "em-text", [], {"property": "visaInfo", "label": subexpr(env, context, "t", ["routes.new.create.visa_info"], {}), "placeholder": subexpr(env, context, "t", ["routes.new.create.visa_info_ph"], {}), "rows": 3});
          inline(env, morph1, context, "em-input", [], {"property": "governmentPage", "label": subexpr(env, context, "t", ["routes.new.create.link_to_government_page"], {}), "placeholder": subexpr(env, context, "t", ["routes.new.create.link_to_government_page_ph"], {}), "type": "url"});
          inline(env, morph2, context, "em-text", [], {"property": "description", "label": subexpr(env, context, "t", ["routes.new.create.descryption_of_the_country"], {}), "placeholder": subexpr(env, context, "t", ["routes.new.create.descryption_ph"], {}), "rows": 3});
          inline(env, morph3, context, "em-text", [], {"property": "transportInfo", "label": subexpr(env, context, "t", ["routes.new.create.transport_information"], {}), "placeholder": subexpr(env, context, "t", ["routes.new.create.transport_information_ph"], {}), "rows": 3});
          inline(env, morph4, context, "t", ["routes.new.create.main_transport_hubs"], {});
          inline(env, morph5, context, "partial", ["routes/new/partials/each-transport-hub"], {});
          element(env, element2, context, "action", ["addInsertion", "transportHub"], {});
          inline(env, morph6, context, "t", ["routes.new.add_transport_hub"], {});
          inline(env, morph7, context, "t", ["routes.new.create.important_phone_numbers"], {});
          inline(env, morph8, context, "partial", ["routes/new/partials/each-contact"], {});
          element(env, element3, context, "action", ["addInsertion", "contact"], {});
          inline(env, morph9, context, "t", ["routes.new.add_phone_number"], {});
          element(env, element4, context, "action", ["addInsertion", "photo"], {});
          block(env, morph10, context, "em-gallery", [], {"items": get(env, context, "photos")}, child0, null);
          block(env, morph11, context, "link-to", ["routes.new.country"], {"class": "button alert"}, child1, null);
          element(env, element6, context, "bind-attr", [], {"class": ":button :success model.isValid::disabled"});
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","map-controls");
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","map-content");
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, subexpr = hooks.subexpr, get = hooks.get, inline = hooks.inline, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(dom.childAt(fragment, [0]),0,0);
        var morph1 = dom.createMorphAt(dom.childAt(fragment, [1]),0,0);
        inline(env, morph0, context, "gm-select", [], {"content": get(env, context, "locationsList"), "optionLabelPath": "content.name", "optionValuePath": "content.name", "prompt": subexpr(env, context, "t", ["routes.new.create.add_country_ph"], {}), "action": "locationNameAdded"});
        block(env, morph1, context, "em-form", [], {"model": get(env, context, "model"), "action": "createLocation", "submit_button": false}, child0, null);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/routes/new/partials/-choose-location-accommodations', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      var child0 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createElement("tr");
            var el2 = dom.createElement("td");
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("td");
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("td");
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("td");
            dom.setAttribute(el2,"style","width: 10px");
            var el3 = dom.createElement("a");
            var el4 = dom.createElement("i");
            dom.setAttribute(el4,"class","fi-pencil");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("td");
            dom.setAttribute(el2,"style","width: 10px");
            var el3 = dom.createElement("a");
            var el4 = dom.createElement("i");
            dom.setAttribute(el4,"class","fi-x");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, content = hooks.content, get = hooks.get, element = hooks.element;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var element0 = dom.childAt(fragment, [0]);
            var element1 = dom.childAt(element0, [3, 0]);
            var element2 = dom.childAt(element0, [4, 0]);
            var morph0 = dom.createMorphAt(dom.childAt(element0, [0]),0,0);
            var morph1 = dom.createMorphAt(dom.childAt(element0, [1]),0,0);
            var morph2 = dom.createMorphAt(dom.childAt(element0, [2]),0,0);
            content(env, morph0, context, "accommodation.hubType");
            content(env, morph1, context, "accommodation.name");
            content(env, morph2, context, "accommodation.description");
            element(env, element1, context, "action", ["editInsertion", get(env, context, "accommodation")], {});
            element(env, element2, context, "action", ["destroyInsertion", get(env, context, "accommodation")], {});
            return fragment;
          }
        };
      }());
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("table");
          dom.setAttribute(el1,"style","width: 100%");
          var el2 = dom.createElement("tbody");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, get = hooks.get, block = hooks.block;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(dom.childAt(fragment, [0, 0]),0,0);
          block(env, morph0, context, "each", [get(env, context, "location.accommodations")], {"keyword": "accommodation"}, child0, null);
          return fragment;
        }
      };
    }());
    var child1 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("No ACCOMMODATIONS added");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","row collapse");
        var el2 = dom.createElement("div");
        dom.setAttribute(el2,"class","large-10 columns large-centered text-center");
        var el3 = dom.createElement("h3");
        dom.setAttribute(el3,"class","subheader");
        var el4 = dom.createTextNode("ACCOMMODATIONS");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","row collapse");
        var el2 = dom.createElement("div");
        dom.setAttribute(el2,"class","large-10 columns large-centered");
        var el3 = dom.createElement("a");
        dom.setAttribute(el3,"class","button small right");
        var el4 = dom.createTextNode("Add ACCOMMODATION");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, block = hooks.block, element = hooks.element;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var element3 = dom.childAt(fragment, [1, 0, 0]);
        var morph0 = dom.createMorphAt(dom.childAt(fragment, [0, 0]),1,1);
        block(env, morph0, context, "if", [get(env, context, "location.accommodations")], {}, child0, child1);
        element(env, element3, context, "action", ["addInsertion", "accommodation"], {});
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/routes/new/partials/-choose-location-contacts', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("CONTACTS");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          return fragment;
        }
      };
    }());
    var child1 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("Important PHONE NUMBERS");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          return fragment;
        }
      };
    }());
    var child2 = (function() {
      var child0 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createElement("tr");
            var el2 = dom.createElement("td");
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("td");
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("td");
            dom.setAttribute(el2,"style","width: 10px");
            var el3 = dom.createElement("a");
            var el4 = dom.createElement("i");
            dom.setAttribute(el4,"class","fi-pencil");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("td");
            dom.setAttribute(el2,"style","width: 10px");
            var el3 = dom.createElement("a");
            var el4 = dom.createElement("i");
            dom.setAttribute(el4,"class","fi-x");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, content = hooks.content, get = hooks.get, element = hooks.element;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var element0 = dom.childAt(fragment, [0]);
            var element1 = dom.childAt(element0, [2, 0]);
            var element2 = dom.childAt(element0, [3, 0]);
            var morph0 = dom.createMorphAt(dom.childAt(element0, [0]),0,0);
            var morph1 = dom.createMorphAt(dom.childAt(element0, [1]),0,0);
            content(env, morph0, context, "contact.phone");
            content(env, morph1, context, "contact.description");
            element(env, element1, context, "action", ["editInsertion", get(env, context, "contact")], {});
            element(env, element2, context, "destroyInsertion", [get(env, context, "contact")], {});
            return fragment;
          }
        };
      }());
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("table");
          dom.setAttribute(el1,"style","width: 100%");
          var el2 = dom.createElement("tbody");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, get = hooks.get, block = hooks.block;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(dom.childAt(fragment, [0, 0]),0,0);
          block(env, morph0, context, "each", [get(env, context, "location.contacts")], {"keyword": "contact"}, child0, null);
          return fragment;
        }
      };
    }());
    var child3 = (function() {
      var child0 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("No CONTACTS added");
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            return fragment;
          }
        };
      }());
      var child1 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("No PHONE NUMBERS added");
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            return fragment;
          }
        };
      }());
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, get = hooks.get, block = hooks.block;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
          dom.insertBoundary(fragment, null);
          dom.insertBoundary(fragment, 0);
          block(env, morph0, context, "if", [get(env, context, "isSubregion")], {}, child0, child1);
          return fragment;
        }
      };
    }());
    var child4 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("Add CONTACT");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          return fragment;
        }
      };
    }());
    var child5 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("Add PHONE NUMBER");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","row collapse");
        var el2 = dom.createElement("div");
        dom.setAttribute(el2,"class","large-10 columns large-centered text-center");
        var el3 = dom.createElement("h3");
        dom.setAttribute(el3,"class","subheader");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","row collapse");
        var el2 = dom.createElement("div");
        dom.setAttribute(el2,"class","large-10 columns large-centered");
        var el3 = dom.createElement("a");
        dom.setAttribute(el3,"class","button small right");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("hr");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, block = hooks.block, element = hooks.element;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var element3 = dom.childAt(fragment, [0, 0]);
        var element4 = dom.childAt(fragment, [1, 0, 0]);
        var morph0 = dom.createMorphAt(dom.childAt(element3, [0]),0,0);
        var morph1 = dom.createMorphAt(element3,1,1);
        var morph2 = dom.createMorphAt(element4,0,0);
        block(env, morph0, context, "if", [get(env, context, "isSubregion")], {}, child0, child1);
        block(env, morph1, context, "if", [get(env, context, "location.contacts")], {}, child2, child3);
        element(env, element4, context, "action", ["addInsertion", "contact"], {});
        block(env, morph2, context, "if", [get(env, context, "isSubregion")], {}, child4, child5);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/routes/new/partials/-choose-location-description', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","row collapse");
        var el2 = dom.createElement("div");
        dom.setAttribute(el2,"class","large-10 large-centered columns text-center");
        var el3 = dom.createElement("h3");
        dom.setAttribute(el3,"class","subheader");
        var el4 = dom.createTextNode("Description of the ");
        dom.appendChild(el3, el4);
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("p");
        dom.setAttribute(el3,"class","subheader");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("br");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, inline = hooks.inline;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var element0 = dom.childAt(fragment, [0, 0]);
        var morph0 = dom.createMorphAt(dom.childAt(element0, [0]),1,1);
        var morph1 = dom.createMorphAt(dom.childAt(element0, [1]),0,0);
        inline(env, morph0, context, "em-capitalize", [get(env, context, "locationType")], {});
        inline(env, morph1, context, "editable-property", [], {"property": get(env, context, "location.description"), "propertyName": "description"});
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/routes/new/partials/-choose-location-head', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      var child0 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createComment("");
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, inline = hooks.inline;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
            dom.insertBoundary(fragment, null);
            dom.insertBoundary(fragment, 0);
            inline(env, morph0, context, "t", ["routes.new.next_btn"], {});
            return fragment;
          }
        };
      }());
      var child1 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createComment("");
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, inline = hooks.inline;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
            dom.insertBoundary(fragment, null);
            dom.insertBoundary(fragment, 0);
            inline(env, morph0, context, "t", ["routes.new.change_btn"], {});
            return fragment;
          }
        };
      }());
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","map-controls");
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("div");
          dom.setAttribute(el2,"class","clear-button");
          var el3 = dom.createElement("a");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","map-content");
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("br");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("a");
          dom.setAttribute(el2,"class","button small");
          var el3 = dom.createElement("pre");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          var el4 = dom.createTextNode(" ");
          dom.appendChild(el3, el4);
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, get = hooks.get, inline = hooks.inline, element = hooks.element, block = hooks.block, subexpr = hooks.subexpr;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var element0 = dom.childAt(fragment, [0]);
          var element1 = dom.childAt(element0, [1, 0]);
          var element2 = dom.childAt(fragment, [1]);
          var element3 = dom.childAt(element2, [2]);
          var element4 = dom.childAt(element3, [0]);
          var morph0 = dom.createMorphAt(element0,0,0);
          var morph1 = dom.createMorphAt(element1,0,0);
          var morph2 = dom.createMorphAt(element2,0,0);
          var morph3 = dom.createMorphAt(element4,0,0);
          var morph4 = dom.createMorphAt(element4,2,2);
          inline(env, morph0, context, "gm-select", [], {"content": get(env, context, "model"), "optionLabelPath": "content.name", "optionValuePath": "content.id", "prompt": get(env, context, "selectPrompt"), "defaultSelection": get(env, context, "defaultLocationName")});
          element(env, element1, context, "bind-attr", [], {"class": get(env, context, "nextClasses")});
          element(env, element1, context, "action", ["locationChoose", get(env, context, "locationId")], {});
          block(env, morph1, context, "if", [get(env, context, "isCurrentStep")], {}, child0, child1);
          inline(env, morph2, context, "t", ["routes.new.or"], {});
          element(env, element3, context, "action", ["createLocation"], {"on": "click"});
          inline(env, morph3, context, "t", ["add"], {});
          inline(env, morph4, context, "em-capitalize", [subexpr(env, context, "t", [get(env, context, "locationType")], {})], {});
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, block = hooks.block, content = hooks.content;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        var morph1 = dom.createMorphAt(fragment,1,1,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        block(env, morph0, context, "unless", [get(env, context, "isNewLocation")], {}, child0, null);
        content(env, morph1, context, "outlet");
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/routes/new/partials/-choose-location-name', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","row collapse");
        var el2 = dom.createElement("div");
        dom.setAttribute(el2,"class","large-10 large-centered columns text-center");
        var el3 = dom.createElement("h1");
        dom.setAttribute(el3,"class","subheader");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("br");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, content = hooks.content;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(dom.childAt(fragment, [0, 0, 0]),0,0);
        content(env, morph0, context, "location.name");
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/routes/new/partials/-choose-location-transport-hubs', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, inline = hooks.inline;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
          dom.insertBoundary(fragment, null);
          dom.insertBoundary(fragment, 0);
          inline(env, morph0, context, "t", ["routes.new.choose.main_transport_hubs"], {});
          return fragment;
        }
      };
    }());
    var child1 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, inline = hooks.inline;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
          dom.insertBoundary(fragment, null);
          dom.insertBoundary(fragment, 0);
          inline(env, morph0, context, "t", ["routes.new.how_to_get"], {});
          return fragment;
        }
      };
    }());
    var child2 = (function() {
      var child0 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createElement("tr");
            var el2 = dom.createElement("td");
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("td");
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("td");
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("td");
            dom.setAttribute(el2,"style","width: 10px");
            var el3 = dom.createElement("a");
            var el4 = dom.createElement("i");
            dom.setAttribute(el4,"class","fi-pencil");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("td");
            dom.setAttribute(el2,"style","width: 10px");
            var el3 = dom.createElement("a");
            var el4 = dom.createElement("i");
            dom.setAttribute(el4,"class","fi-x");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, content = hooks.content, get = hooks.get, element = hooks.element;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var element0 = dom.childAt(fragment, [0]);
            var element1 = dom.childAt(element0, [3, 0]);
            var element2 = dom.childAt(element0, [4, 0]);
            var morph0 = dom.createMorphAt(dom.childAt(element0, [0]),0,0);
            var morph1 = dom.createMorphAt(dom.childAt(element0, [1]),0,0);
            var morph2 = dom.createMorphAt(dom.childAt(element0, [2]),0,0);
            content(env, morph0, context, "th.hubType");
            content(env, morph1, context, "th.name");
            content(env, morph2, context, "th.description");
            element(env, element1, context, "action", ["editInsertion", get(env, context, "th")], {});
            element(env, element2, context, "action", ["destroyInsertion", get(env, context, "th")], {});
            return fragment;
          }
        };
      }());
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("table");
          dom.setAttribute(el1,"style","width: 100%");
          var el2 = dom.createElement("tbody");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, get = hooks.get, block = hooks.block;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(dom.childAt(fragment, [0, 0]),0,0);
          block(env, morph0, context, "each", [get(env, context, "location.transportHubs")], {"keyword": "th"}, child0, null);
          return fragment;
        }
      };
    }());
    var child3 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, inline = hooks.inline;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
          dom.insertBoundary(fragment, null);
          dom.insertBoundary(fragment, 0);
          inline(env, morph0, context, "t", ["routes.new.no_transport_hubs_added"], {});
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","row collapse");
        var el2 = dom.createElement("div");
        dom.setAttribute(el2,"class","large-10 columns large-centered text-center");
        var el3 = dom.createElement("h3");
        dom.setAttribute(el3,"class","subheader");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","row collapse");
        var el2 = dom.createElement("div");
        dom.setAttribute(el2,"class","large-10 columns large-centered");
        var el3 = dom.createElement("a");
        dom.setAttribute(el3,"class","button small right");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, block = hooks.block, element = hooks.element, inline = hooks.inline;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var element3 = dom.childAt(fragment, [0, 0]);
        var element4 = dom.childAt(fragment, [1, 0, 0]);
        var morph0 = dom.createMorphAt(dom.childAt(element3, [0]),0,0);
        var morph1 = dom.createMorphAt(element3,1,1);
        var morph2 = dom.createMorphAt(element4,0,0);
        block(env, morph0, context, "if", [get(env, context, "isCountry")], {}, child0, child1);
        block(env, morph1, context, "if", [get(env, context, "location.transportHubs")], {}, child2, child3);
        element(env, element4, context, "action", ["addInsertion", "transportHub"], {});
        inline(env, morph2, context, "t", ["routes.new.add_transport_hub"], {});
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/routes/new/partials/-choose-location-transport-info', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","row collapse");
        var el2 = dom.createElement("div");
        dom.setAttribute(el2,"class","large-10 large-centered columns text-center");
        var el3 = dom.createElement("h3");
        dom.setAttribute(el3,"class","subheader");
        var el4 = dom.createTextNode(" TRANSPORT Information");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("p");
        dom.setAttribute(el3,"class","subheader");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("br");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, inline = hooks.inline;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(dom.childAt(fragment, [0, 0, 1]),0,0);
        inline(env, morph0, context, "editable-property", [], {"property": get(env, context, "location.transportInfo"), "propertyName": "transportInfo"});
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/routes/new/partials/-choose-location-visa', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","row collapse");
        var el2 = dom.createElement("div");
        dom.setAttribute(el2,"class","large-10 large-centered columns text-center");
        var el3 = dom.createElement("h3");
        dom.setAttribute(el3,"class","subheader");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("p");
        dom.setAttribute(el3,"class","subheader");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("a");
        dom.setAttribute(el4,"target","_blank");
        var el5 = dom.createComment("");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("br");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, inline = hooks.inline, get = hooks.get, element = hooks.element;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var element0 = dom.childAt(fragment, [0, 0]);
        var element1 = dom.childAt(element0, [1]);
        var element2 = dom.childAt(element1, [1]);
        var morph0 = dom.createMorphAt(dom.childAt(element0, [0]),0,0);
        var morph1 = dom.createMorphAt(element1,0,0);
        var morph2 = dom.createMorphAt(element2,0,0);
        inline(env, morph0, context, "t", ["routes.new.choose.visa_info"], {});
        inline(env, morph1, context, "editable-property", [], {"property": get(env, context, "location.visaInfo"), "propertyName": "visaInfo"});
        element(env, element2, context, "bind-attr", [], {"href": get(env, context, "location.governmentPage")});
        inline(env, morph2, context, "t", ["routes.new.choose.link_to_government_page"], {});
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/routes/new/partials/-choose-location', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      var child0 = (function() {
        var child0 = (function() {
          return {
            isHTMLBars: true,
            revision: "Ember@1.11.0",
            blockParams: 0,
            cachedFragment: null,
            hasRendered: false,
            build: function build(dom) {
              var el0 = dom.createDocumentFragment();
              var el1 = dom.createElement("tr");
              var el2 = dom.createElement("td");
              var el3 = dom.createComment("");
              dom.appendChild(el2, el3);
              dom.appendChild(el1, el2);
              var el2 = dom.createElement("td");
              var el3 = dom.createComment("");
              dom.appendChild(el2, el3);
              dom.appendChild(el1, el2);
              var el2 = dom.createElement("td");
              var el3 = dom.createComment("");
              dom.appendChild(el2, el3);
              dom.appendChild(el1, el2);
              var el2 = dom.createElement("td");
              dom.setAttribute(el2,"style","width: 10px");
              var el3 = dom.createElement("a");
              var el4 = dom.createElement("i");
              dom.setAttribute(el4,"class","fi-pencil");
              dom.appendChild(el3, el4);
              dom.appendChild(el2, el3);
              dom.appendChild(el1, el2);
              var el2 = dom.createElement("td");
              dom.setAttribute(el2,"style","width: 10px");
              var el3 = dom.createElement("a");
              var el4 = dom.createElement("i");
              dom.setAttribute(el4,"class","fi-x");
              dom.appendChild(el3, el4);
              dom.appendChild(el2, el3);
              dom.appendChild(el1, el2);
              dom.appendChild(el0, el1);
              return el0;
            },
            render: function render(context, env, contextualElement) {
              var dom = env.dom;
              var hooks = env.hooks, content = hooks.content, get = hooks.get, element = hooks.element;
              dom.detectNamespace(contextualElement);
              var fragment;
              if (env.useFragmentCache && dom.canClone) {
                if (this.cachedFragment === null) {
                  fragment = this.build(dom);
                  if (this.hasRendered) {
                    this.cachedFragment = fragment;
                  } else {
                    this.hasRendered = true;
                  }
                }
                if (this.cachedFragment) {
                  fragment = dom.cloneNode(this.cachedFragment, true);
                }
              } else {
                fragment = this.build(dom);
              }
              var element3 = dom.childAt(fragment, [0]);
              var element4 = dom.childAt(element3, [3, 0]);
              var element5 = dom.childAt(element3, [4, 0]);
              var morph0 = dom.createMorphAt(dom.childAt(element3, [0]),0,0);
              var morph1 = dom.createMorphAt(dom.childAt(element3, [1]),0,0);
              var morph2 = dom.createMorphAt(dom.childAt(element3, [2]),0,0);
              content(env, morph0, context, "th.hubType");
              content(env, morph1, context, "th.name");
              content(env, morph2, context, "th.description");
              element(env, element4, context, "action", [get(env, context, "editInsertion"), get(env, context, "th")], {"on": "click"});
              element(env, element5, context, "action", [get(env, context, "destroyInsertion"), get(env, context, "th")], {"on": "click"});
              return fragment;
            }
          };
        }());
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createElement("table");
            dom.setAttribute(el1,"style","width: 100%");
            var el2 = dom.createElement("tbody");
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, get = hooks.get, block = hooks.block;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var morph0 = dom.createMorphAt(dom.childAt(fragment, [0, 0]),0,0);
            block(env, morph0, context, "each", [get(env, context, "location.transportHubs")], {"keyword": "th"}, child0, null);
            return fragment;
          }
        };
      }());
      var child1 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("No TRANSPORT HUBS added");
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            return fragment;
          }
        };
      }());
      var child2 = (function() {
        var child0 = (function() {
          return {
            isHTMLBars: true,
            revision: "Ember@1.11.0",
            blockParams: 0,
            cachedFragment: null,
            hasRendered: false,
            build: function build(dom) {
              var el0 = dom.createDocumentFragment();
              var el1 = dom.createElement("tr");
              var el2 = dom.createElement("td");
              var el3 = dom.createComment("");
              dom.appendChild(el2, el3);
              dom.appendChild(el1, el2);
              var el2 = dom.createElement("td");
              var el3 = dom.createComment("");
              dom.appendChild(el2, el3);
              dom.appendChild(el1, el2);
              var el2 = dom.createElement("td");
              dom.setAttribute(el2,"style","width: 10px");
              var el3 = dom.createElement("a");
              var el4 = dom.createElement("i");
              dom.setAttribute(el4,"class","fi-pencil");
              dom.appendChild(el3, el4);
              dom.appendChild(el2, el3);
              dom.appendChild(el1, el2);
              var el2 = dom.createElement("td");
              dom.setAttribute(el2,"style","width: 10px");
              var el3 = dom.createElement("a");
              var el4 = dom.createElement("i");
              dom.setAttribute(el4,"class","fi-x");
              dom.appendChild(el3, el4);
              dom.appendChild(el2, el3);
              dom.appendChild(el1, el2);
              dom.appendChild(el0, el1);
              return el0;
            },
            render: function render(context, env, contextualElement) {
              var dom = env.dom;
              var hooks = env.hooks, content = hooks.content, get = hooks.get, element = hooks.element;
              dom.detectNamespace(contextualElement);
              var fragment;
              if (env.useFragmentCache && dom.canClone) {
                if (this.cachedFragment === null) {
                  fragment = this.build(dom);
                  if (this.hasRendered) {
                    this.cachedFragment = fragment;
                  } else {
                    this.hasRendered = true;
                  }
                }
                if (this.cachedFragment) {
                  fragment = dom.cloneNode(this.cachedFragment, true);
                }
              } else {
                fragment = this.build(dom);
              }
              var element0 = dom.childAt(fragment, [0]);
              var element1 = dom.childAt(element0, [2, 0]);
              var element2 = dom.childAt(element0, [3, 0]);
              var morph0 = dom.createMorphAt(dom.childAt(element0, [0]),0,0);
              var morph1 = dom.createMorphAt(dom.childAt(element0, [1]),0,0);
              content(env, morph0, context, "contact.phone");
              content(env, morph1, context, "contact.description");
              element(env, element1, context, "action", [get(env, context, "editInsertion"), get(env, context, "contact")], {"on": "click"});
              element(env, element2, context, "action", [get(env, context, "destroyInsertion"), get(env, context, "contact")], {"on": "click"});
              return fragment;
            }
          };
        }());
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createElement("table");
            dom.setAttribute(el1,"style","width: 100%");
            var el2 = dom.createElement("tbody");
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, get = hooks.get, block = hooks.block;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var morph0 = dom.createMorphAt(dom.childAt(fragment, [0, 0]),0,0);
            block(env, morph0, context, "each", [get(env, context, "location.contacts")], {"keyword": "contact"}, child0, null);
            return fragment;
          }
        };
      }());
      var child3 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("No PHONE NUMBERS added");
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            return fragment;
          }
        };
      }());
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("hr");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","row collapse");
          var el2 = dom.createElement("div");
          dom.setAttribute(el2,"class","large-10 large-centered columns text-center");
          var el3 = dom.createElement("h1");
          dom.setAttribute(el3,"class","subheader");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("br");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","row collapse");
          var el2 = dom.createElement("div");
          dom.setAttribute(el2,"class","large-10 large-centered columns text-center");
          var el3 = dom.createElement("h3");
          dom.setAttribute(el3,"class","subheader");
          var el4 = dom.createTextNode("VISA Info");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("p");
          dom.setAttribute(el3,"class","subheader");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          var el4 = dom.createElement("a");
          dom.setAttribute(el4,"target","_blank");
          var el5 = dom.createTextNode("link to government page");
          dom.appendChild(el4, el5);
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("br");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","row collapse");
          var el2 = dom.createElement("div");
          dom.setAttribute(el2,"class","large-10 large-centered columns text-center");
          var el3 = dom.createElement("h3");
          dom.setAttribute(el3,"class","subheader");
          var el4 = dom.createTextNode("Description of the ");
          dom.appendChild(el3, el4);
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("p");
          dom.setAttribute(el3,"class","subheader");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("br");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","row collapse");
          var el2 = dom.createElement("div");
          dom.setAttribute(el2,"class","large-10 large-centered columns text-center");
          var el3 = dom.createElement("h3");
          dom.setAttribute(el3,"class","subheader");
          var el4 = dom.createTextNode(" TRANSPORT Information");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("p");
          dom.setAttribute(el3,"class","subheader");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("br");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","row collapse");
          var el2 = dom.createElement("div");
          dom.setAttribute(el2,"class","large-10 columns large-centered text-center");
          var el3 = dom.createElement("h3");
          dom.setAttribute(el3,"class","subheader");
          var el4 = dom.createTextNode("Main TRANSPORT HUBS");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","row collapse");
          var el2 = dom.createElement("div");
          dom.setAttribute(el2,"class","large-10 columns large-centered");
          var el3 = dom.createElement("a");
          dom.setAttribute(el3,"class","button small right");
          var el4 = dom.createTextNode("Add TRANSPORT HUB");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","row collapse");
          var el2 = dom.createElement("div");
          dom.setAttribute(el2,"class","large-10 columns large-centered text-center");
          var el3 = dom.createElement("h3");
          dom.setAttribute(el3,"class","subheader");
          var el4 = dom.createTextNode("Important PHONE NUMBERS");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","row collapse");
          var el2 = dom.createElement("div");
          dom.setAttribute(el2,"class","large-10 columns large-centered");
          var el3 = dom.createElement("a");
          dom.setAttribute(el3,"class","button small right");
          var el4 = dom.createTextNode("Add PHONE NUMBER");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("hr");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, content = hooks.content, get = hooks.get, inline = hooks.inline, element = hooks.element, block = hooks.block;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var element6 = dom.childAt(fragment, [2, 0, 1]);
          var element7 = dom.childAt(element6, [1]);
          var element8 = dom.childAt(fragment, [3, 0]);
          var element9 = dom.childAt(fragment, [6, 0, 0]);
          var element10 = dom.childAt(fragment, [8, 0, 0]);
          var morph0 = dom.createMorphAt(dom.childAt(fragment, [1, 0, 0]),0,0);
          var morph1 = dom.createMorphAt(element6,0,0);
          var morph2 = dom.createMorphAt(dom.childAt(element8, [0]),1,1);
          var morph3 = dom.createMorphAt(dom.childAt(element8, [1]),0,0);
          var morph4 = dom.createMorphAt(dom.childAt(fragment, [4, 0, 1]),0,0);
          var morph5 = dom.createMorphAt(dom.childAt(fragment, [5, 0]),1,1);
          var morph6 = dom.createMorphAt(dom.childAt(fragment, [7, 0]),1,1);
          content(env, morph0, context, "location.name");
          inline(env, morph1, context, "editable-property", [], {"property": get(env, context, "location.visaInfo"), "propertyName": "visaInfo"});
          element(env, element7, context, "bind-attr", [], {"href": get(env, context, "location.governmentPage")});
          content(env, morph2, context, "camelizedLocationType");
          inline(env, morph3, context, "editable-property", [], {"property": get(env, context, "location.description"), "propertyName": "description"});
          inline(env, morph4, context, "editable-property", [], {"property": get(env, context, "location.transportInfo"), "propertyName": "transportInfo"});
          block(env, morph5, context, "if", [get(env, context, "location.transportHubs")], {}, child0, child1);
          element(env, element9, context, "action", [get(env, context, "addInsertion"), "transportHub"], {"on": "click"});
          block(env, morph6, context, "if", [get(env, context, "location.contacts")], {}, child2, child3);
          element(env, element10, context, "action", [get(env, context, "addInsertion"), "contact"], {"on": "click"});
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","row");
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, inline = hooks.inline, get = hooks.get, block = hooks.block, content = hooks.content;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        var morph1 = dom.createMorphAt(fragment,1,1,contextualElement);
        var morph2 = dom.createMorphAt(dom.childAt(fragment, [2]),0,0);
        dom.insertBoundary(fragment, 0);
        inline(env, morph0, context, "partial", ["routes/new/choose-location-head"], {});
        block(env, morph1, context, "if", [get(env, context, "isLocationChoosed")], {}, child0, null);
        content(env, morph2, context, "outlet");
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/routes/new/partials/-each-contact', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      var child0 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createElement("tr");
            var el2 = dom.createElement("td");
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("td");
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("td");
            dom.setAttribute(el2,"style","width: 10px");
            var el3 = dom.createElement("a");
            var el4 = dom.createElement("i");
            dom.setAttribute(el4,"class","fi-pencil");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("td");
            dom.setAttribute(el2,"style","width: 10px");
            var el3 = dom.createElement("a");
            var el4 = dom.createElement("i");
            dom.setAttribute(el4,"class","fi-x");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, content = hooks.content, get = hooks.get, element = hooks.element;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var element0 = dom.childAt(fragment, [0]);
            var element1 = dom.childAt(element0, [2, 0]);
            var element2 = dom.childAt(element0, [3, 0]);
            var morph0 = dom.createMorphAt(dom.childAt(element0, [0]),0,0);
            var morph1 = dom.createMorphAt(dom.childAt(element0, [1]),0,0);
            content(env, morph0, context, "contact.phone");
            content(env, morph1, context, "contact.description");
            element(env, element1, context, "action", ["editInsertion", get(env, context, "contact")], {});
            element(env, element2, context, "action", ["destroyInsertion", get(env, context, "contact")], {});
            return fragment;
          }
        };
      }());
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("table");
          dom.setAttribute(el1,"style","width: 100%");
          var el2 = dom.createElement("tbody");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, get = hooks.get, block = hooks.block;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(dom.childAt(fragment, [0, 0]),0,0);
          block(env, morph0, context, "each", [get(env, context, "model.contacts")], {"keyword": "contact"}, child0, null);
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        block(env, morph0, context, "if", [get(env, context, "model.contacts")], {}, child0, null);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/routes/new/partials/-each-transport-hub', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      var child0 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createElement("tr");
            var el2 = dom.createElement("td");
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("td");
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("td");
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("td");
            dom.setAttribute(el2,"style","width: 10px");
            var el3 = dom.createElement("a");
            var el4 = dom.createElement("i");
            dom.setAttribute(el4,"class","fi-pencil");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("td");
            dom.setAttribute(el2,"style","width: 10px");
            var el3 = dom.createElement("a");
            var el4 = dom.createElement("i");
            dom.setAttribute(el4,"class","fi-x");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, content = hooks.content, get = hooks.get, element = hooks.element;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var element0 = dom.childAt(fragment, [0]);
            var element1 = dom.childAt(element0, [3, 0]);
            var element2 = dom.childAt(element0, [4, 0]);
            var morph0 = dom.createMorphAt(dom.childAt(element0, [0]),0,0);
            var morph1 = dom.createMorphAt(dom.childAt(element0, [1]),0,0);
            var morph2 = dom.createMorphAt(dom.childAt(element0, [2]),0,0);
            content(env, morph0, context, "th.hubType");
            content(env, morph1, context, "th.name");
            content(env, morph2, context, "th.description");
            element(env, element1, context, "action", ["editInsertion", get(env, context, "th")], {});
            element(env, element2, context, "action", ["destroyInsertion", get(env, context, "th")], {});
            return fragment;
          }
        };
      }());
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("table");
          dom.setAttribute(el1,"style","width: 100%");
          var el2 = dom.createElement("tbody");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, get = hooks.get, block = hooks.block;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(dom.childAt(fragment, [0, 0]),0,0);
          block(env, morph0, context, "each", [get(env, context, "model.transportHubs")], {"keyword": "th"}, child0, null);
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        block(env, morph0, context, "if", [get(env, context, "model.transportHubs")], {}, child0, null);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/routes/new/region', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("hr");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, inline = hooks.inline;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(fragment,1,1,contextualElement);
          var morph1 = dom.createMorphAt(fragment,2,2,contextualElement);
          var morph2 = dom.createMorphAt(fragment,3,3,contextualElement);
          var morph3 = dom.createMorphAt(fragment,4,4,contextualElement);
          dom.insertBoundary(fragment, null);
          inline(env, morph0, context, "partial", ["routes/new/partials/choose-location-name"], {});
          inline(env, morph1, context, "partial", ["routes/new/partials/choose-location-description"], {});
          inline(env, morph2, context, "partial", ["routes/new/partials/choose-location-transport-hubs"], {});
          inline(env, morph3, context, "partial", ["routes/new/partials/choose-location-contacts"], {});
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, inline = hooks.inline, get = hooks.get, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        var morph1 = dom.createMorphAt(fragment,1,1,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        inline(env, morph0, context, "partial", ["routes/new/partials/choose-location-head"], {});
        block(env, morph1, context, "if", [get(env, context, "isLocationChoosed")], {}, child0, null);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/routes/new/region/create', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      var child0 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createComment("");
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, inline = hooks.inline;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
            dom.insertBoundary(fragment, null);
            dom.insertBoundary(fragment, 0);
            inline(env, morph0, context, "t", ["cancel"], {});
            return fragment;
          }
        };
      }());
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","row collapse");
          var el2 = dom.createElement("div");
          dom.setAttribute(el2,"class","large-10");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("label");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("a");
          dom.setAttribute(el3,"class","button small");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("label");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("a");
          dom.setAttribute(el3,"class","button small");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","large-6 offset-3");
          var el2 = dom.createElement("div");
          dom.setAttribute(el2,"class","form-actions");
          var el3 = dom.createElement("br");
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("input");
          dom.setAttribute(el3,"type","submit");
          dom.setAttribute(el3,"value","Save");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, subexpr = hooks.subexpr, inline = hooks.inline, element = hooks.element, block = hooks.block;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var element0 = dom.childAt(fragment, [0, 0]);
          var element1 = dom.childAt(element0, [3]);
          var element2 = dom.childAt(element0, [6]);
          var element3 = dom.childAt(fragment, [1, 0]);
          var element4 = dom.childAt(element3, [2]);
          var morph0 = dom.createMorphAt(element0,0,0);
          var morph1 = dom.createMorphAt(dom.childAt(element0, [1]),0,0);
          var morph2 = dom.createMorphAt(element0,2,2);
          var morph3 = dom.createMorphAt(element1,0,0);
          var morph4 = dom.createMorphAt(dom.childAt(element0, [4]),0,0);
          var morph5 = dom.createMorphAt(element0,5,5);
          var morph6 = dom.createMorphAt(element2,0,0);
          var morph7 = dom.createMorphAt(element3,1,1);
          inline(env, morph0, context, "em-text", [], {"property": "description", "label": subexpr(env, context, "t", ["routes.new.create.descryption_of_the_region"], {}), "placeholder": subexpr(env, context, "t", ["routes.new.create.descryption_ph"], {}), "rows": 3});
          inline(env, morph1, context, "t", ["routes.new.create.how_to_get"], {});
          inline(env, morph2, context, "partial", ["routes/new/partials/each-transport-hub"], {});
          element(env, element1, context, "action", ["addInsertion", "transportHub"], {});
          inline(env, morph3, context, "t", ["routes.new.add_transport_hub"], {});
          inline(env, morph4, context, "t", ["routes.new.create.important_phone_numbers"], {});
          inline(env, morph5, context, "partial", ["routes/new/partials/each-contact"], {});
          element(env, element2, context, "action", ["addInsertion", "contact"], {});
          inline(env, morph6, context, "t", ["routes.new.add_phone_number"], {});
          block(env, morph7, context, "link-to", ["routes.new.region"], {"class": "button alert"}, child0, null);
          element(env, element4, context, "bind-attr", [], {"class": ":button :success model.isValid::disabled"});
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","map-controls");
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","map-content");
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, subexpr = hooks.subexpr, get = hooks.get, inline = hooks.inline, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(dom.childAt(fragment, [0]),0,0);
        var morph1 = dom.createMorphAt(dom.childAt(fragment, [1]),0,0);
        inline(env, morph0, context, "gm-location-autocomplite", [], {"location": get(env, context, "model"), "optionLabelPath": "content", "optionValuePath": "content", "prompt": subexpr(env, context, "t", ["routes.new.create.add_region_ph"], {})});
        block(env, morph1, context, "em-form", [], {"model": get(env, context, "model"), "action": "createLocation", "submit_button": false}, child0, null);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/routes/new/subregion', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("hr");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, inline = hooks.inline;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var morph0 = dom.createMorphAt(fragment,1,1,contextualElement);
          var morph1 = dom.createMorphAt(fragment,2,2,contextualElement);
          var morph2 = dom.createMorphAt(fragment,3,3,contextualElement);
          var morph3 = dom.createMorphAt(fragment,4,4,contextualElement);
          var morph4 = dom.createMorphAt(fragment,5,5,contextualElement);
          dom.insertBoundary(fragment, null);
          inline(env, morph0, context, "partial", ["routes/new/partials/choose-location-name"], {});
          inline(env, morph1, context, "partial", ["routes/new/partials/choose-location-description"], {});
          inline(env, morph2, context, "partial", ["routes/new/partials/choose-location-transport-hubs"], {});
          inline(env, morph3, context, "partial", ["routes/new/partials/choose-location-accommodations"], {});
          inline(env, morph4, context, "partial", ["routes/new/partials/choose-location-contacts"], {});
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, inline = hooks.inline, get = hooks.get, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        var morph1 = dom.createMorphAt(fragment,1,1,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        inline(env, morph0, context, "partial", ["routes/new/partials/choose-location-head"], {});
        block(env, morph1, context, "if", [get(env, context, "isLocationChoosed")], {}, child0, null);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/routes/new/subregion/create', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      var child0 = (function() {
        var child0 = (function() {
          return {
            isHTMLBars: true,
            revision: "Ember@1.11.0",
            blockParams: 0,
            cachedFragment: null,
            hasRendered: false,
            build: function build(dom) {
              var el0 = dom.createDocumentFragment();
              var el1 = dom.createElement("tr");
              var el2 = dom.createElement("td");
              var el3 = dom.createComment("");
              dom.appendChild(el2, el3);
              dom.appendChild(el1, el2);
              var el2 = dom.createElement("td");
              var el3 = dom.createComment("");
              dom.appendChild(el2, el3);
              dom.appendChild(el1, el2);
              var el2 = dom.createElement("td");
              var el3 = dom.createComment("");
              dom.appendChild(el2, el3);
              dom.appendChild(el1, el2);
              var el2 = dom.createElement("td");
              dom.setAttribute(el2,"style","width: 10px");
              var el3 = dom.createElement("a");
              var el4 = dom.createElement("i");
              dom.setAttribute(el4,"class","fi-pencil");
              dom.appendChild(el3, el4);
              dom.appendChild(el2, el3);
              dom.appendChild(el1, el2);
              var el2 = dom.createElement("td");
              dom.setAttribute(el2,"style","width: 10px");
              var el3 = dom.createElement("a");
              var el4 = dom.createElement("i");
              dom.setAttribute(el4,"class","fi-x");
              dom.appendChild(el3, el4);
              dom.appendChild(el2, el3);
              dom.appendChild(el1, el2);
              dom.appendChild(el0, el1);
              return el0;
            },
            render: function render(context, env, contextualElement) {
              var dom = env.dom;
              var hooks = env.hooks, content = hooks.content, get = hooks.get, element = hooks.element;
              dom.detectNamespace(contextualElement);
              var fragment;
              if (env.useFragmentCache && dom.canClone) {
                if (this.cachedFragment === null) {
                  fragment = this.build(dom);
                  if (this.hasRendered) {
                    this.cachedFragment = fragment;
                  } else {
                    this.hasRendered = true;
                  }
                }
                if (this.cachedFragment) {
                  fragment = dom.cloneNode(this.cachedFragment, true);
                }
              } else {
                fragment = this.build(dom);
              }
              var element0 = dom.childAt(fragment, [0]);
              var element1 = dom.childAt(element0, [3, 0]);
              var element2 = dom.childAt(element0, [4, 0]);
              var morph0 = dom.createMorphAt(dom.childAt(element0, [0]),0,0);
              var morph1 = dom.createMorphAt(dom.childAt(element0, [1]),0,0);
              var morph2 = dom.createMorphAt(dom.childAt(element0, [2]),0,0);
              content(env, morph0, context, "accommodation.accommodationType");
              content(env, morph1, context, "accommodation.name");
              content(env, morph2, context, "accommodation.description");
              element(env, element1, context, "action", ["editInsertion", get(env, context, "accommodation")], {});
              element(env, element2, context, "action", ["destroyInsertion", get(env, context, "accommodation")], {});
              return fragment;
            }
          };
        }());
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createElement("table");
            dom.setAttribute(el1,"style","width: 100%");
            var el2 = dom.createElement("tbody");
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, get = hooks.get, block = hooks.block;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var morph0 = dom.createMorphAt(dom.childAt(fragment, [0, 0]),0,0);
            block(env, morph0, context, "each", [get(env, context, "model.accommodations")], {"keyword": "accommodation"}, child0, null);
            return fragment;
          }
        };
      }());
      var child1 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createComment("");
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, inline = hooks.inline;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
            dom.insertBoundary(fragment, null);
            dom.insertBoundary(fragment, 0);
            inline(env, morph0, context, "t", ["cancel"], {});
            return fragment;
          }
        };
      }());
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","row collapse");
          var el2 = dom.createElement("div");
          dom.setAttribute(el2,"class","large-10");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("label");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("a");
          dom.setAttribute(el3,"class","button small");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("label");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("a");
          dom.setAttribute(el3,"class","button small");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("label");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("a");
          dom.setAttribute(el3,"class","button small");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","large-6 offset-3");
          var el2 = dom.createElement("div");
          dom.setAttribute(el2,"class","form-actions");
          var el3 = dom.createElement("br");
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("input");
          dom.setAttribute(el3,"type","submit");
          dom.setAttribute(el3,"value","Save");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, subexpr = hooks.subexpr, inline = hooks.inline, element = hooks.element, get = hooks.get, block = hooks.block;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var element3 = dom.childAt(fragment, [0, 0]);
          var element4 = dom.childAt(element3, [3]);
          var element5 = dom.childAt(element3, [6]);
          var element6 = dom.childAt(element3, [9]);
          var element7 = dom.childAt(fragment, [1, 0]);
          var element8 = dom.childAt(element7, [2]);
          var morph0 = dom.createMorphAt(element3,0,0);
          var morph1 = dom.createMorphAt(dom.childAt(element3, [1]),0,0);
          var morph2 = dom.createMorphAt(element3,2,2);
          var morph3 = dom.createMorphAt(element4,0,0);
          var morph4 = dom.createMorphAt(dom.childAt(element3, [4]),0,0);
          var morph5 = dom.createMorphAt(element3,5,5);
          var morph6 = dom.createMorphAt(element5,0,0);
          var morph7 = dom.createMorphAt(dom.childAt(element3, [7]),0,0);
          var morph8 = dom.createMorphAt(element3,8,8);
          var morph9 = dom.createMorphAt(element6,0,0);
          var morph10 = dom.createMorphAt(element7,1,1);
          inline(env, morph0, context, "em-text", [], {"property": "description", "label": subexpr(env, context, "t", ["routes.new.create.descryption_of_the_subregion"], {}), "placeholder": subexpr(env, context, "t", ["routes.new.create.descryption_ph"], {}), "rows": 3});
          inline(env, morph1, context, "t", ["routes.new.create.how_to_get"], {});
          inline(env, morph2, context, "partial", ["routes/new/partials/each-transport-hub"], {});
          element(env, element4, context, "action", ["addInsertion", "transportHub"], {});
          inline(env, morph3, context, "t", ["routes.new.add_transport_hub"], {});
          inline(env, morph4, context, "t", ["routes.new.accommodations"], {});
          block(env, morph5, context, "if", [get(env, context, "model.accommodations")], {}, child0, null);
          element(env, element5, context, "action", ["addInsertion", "accommodation"], {});
          inline(env, morph6, context, "t", ["routes.new.add_.accommodation"], {});
          inline(env, morph7, context, "t", ["routes.new.contacts"], {});
          inline(env, morph8, context, "partial", ["routes/new/partials/each-contact"], {});
          element(env, element6, context, "action", ["addInsertion", "contact"], {});
          inline(env, morph9, context, "t", ["routes.new.add_contact"], {});
          block(env, morph10, context, "link-to", ["routes.new.subregion"], {"class": "button alert"}, child1, null);
          element(env, element8, context, "bind-attr", [], {"class": ":button :success model.isValid::disabled"});
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","map-controls");
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","map-content");
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, subexpr = hooks.subexpr, get = hooks.get, inline = hooks.inline, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(dom.childAt(fragment, [0]),0,0);
        var morph1 = dom.createMorphAt(dom.childAt(fragment, [1]),0,0);
        inline(env, morph0, context, "gm-location-autocomplite", [], {"location": get(env, context, "model"), "optionLabelPath": "content", "optionValuePath": "content", "prompt": subexpr(env, context, "t", ["routes.new.create.add_subregion_ph"], {})});
        block(env, morph1, context, "em-form", [], {"model": get(env, context, "model"), "action": "createLocation", "submit_button": false}, child0, null);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/typed-insertion/add-accommodation', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("label");
          var el2 = dom.createTextNode("Choose accommodation type");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","form-actions");
          var el2 = dom.createElement("br");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("input");
          dom.setAttribute(el2,"type","submit");
          dom.setAttribute(el2,"value","Add ACCOMMODATION");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, get = hooks.get, inline = hooks.inline, element = hooks.element;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var element0 = dom.childAt(fragment, [4, 1]);
          var morph0 = dom.createMorphAt(fragment,1,1,contextualElement);
          var morph1 = dom.createMorphAt(fragment,2,2,contextualElement);
          var morph2 = dom.createMorphAt(fragment,3,3,contextualElement);
          inline(env, morph0, context, "simple-select", [], {"content": get(env, context, "accommodationTypes"), "action": "insertionSelectChanged", "prompt": "Please choose a accommodation type", "value": get(env, context, "typedInsertion.insertionType")});
          inline(env, morph1, context, "em-input", [], {"property": "name", "label": "Accommodation NAME", "placeholder": "Add accommodation name...", "type": "text"});
          inline(env, morph2, context, "em-text", [], {"property": "description", "label": "Accommodation DESCRIPTION", "placeholder": "Add accommodation description...", "rows": 3});
          element(env, element0, context, "bind-attr", [], {"class": ":button :success typedInsertion.isValid::disabled"});
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        block(env, morph0, context, "em-form", [], {"model": get(env, context, "typedInsertion"), "action": "createAccommodation", "submit_button": false}, child0, null);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/typed-insertion/add-contact', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","form-actions");
          var el2 = dom.createElement("br");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("input");
          dom.setAttribute(el2,"type","submit");
          dom.setAttribute(el2,"value","Add PHONE NUMBER");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, inline = hooks.inline, element = hooks.element;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var element0 = dom.childAt(fragment, [2, 1]);
          var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
          var morph1 = dom.createMorphAt(fragment,1,1,contextualElement);
          dom.insertBoundary(fragment, 0);
          inline(env, morph0, context, "em-input", [], {"property": "phone", "label": "PHONE NUMBER", "placeholder": "Add phone number...", "type": "text"});
          inline(env, morph1, context, "em-text", [], {"property": "description", "label": "DESCRIPTION", "placeholder": "Add phone number description...", "rows": 3});
          element(env, element0, context, "bind-attr", [], {"class": ":button :success typedInsertion.isValid::disabled"});
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        block(env, morph0, context, "em-form", [], {"model": get(env, context, "typedInsertion"), "action": "createContact", "submit_button": false}, child0, null);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/typed-insertion/add-photo', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      var child0 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createElement("img");
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            var hooks = env.hooks, element = hooks.element;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            var element0 = dom.childAt(fragment, [0]);
            element(env, element0, context, "bind-attr", [], {"src": "dataURL"});
            return fragment;
          }
        };
      }());
      var child1 = (function() {
        return {
          isHTMLBars: true,
          revision: "Ember@1.11.0",
          blockParams: 0,
          cachedFragment: null,
          hasRendered: false,
          build: function build(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("Choose PHOTO");
            dom.appendChild(el0, el1);
            return el0;
          },
          render: function render(context, env, contextualElement) {
            var dom = env.dom;
            dom.detectNamespace(contextualElement);
            var fragment;
            if (env.useFragmentCache && dom.canClone) {
              if (this.cachedFragment === null) {
                fragment = this.build(dom);
                if (this.hasRendered) {
                  this.cachedFragment = fragment;
                } else {
                  this.hasRendered = true;
                }
              }
              if (this.cachedFragment) {
                fragment = dom.cloneNode(this.cachedFragment, true);
              }
            } else {
              fragment = this.build(dom);
            }
            return fragment;
          }
        };
      }());
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","row");
          var el2 = dom.createElement("div");
          dom.setAttribute(el2,"class","twelve columns text-center");
          var el3 = dom.createElement("br");
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("div");
          dom.setAttribute(el3,"class","right");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","row");
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("div");
          dom.setAttribute(el2,"class","form-actions");
          var el3 = dom.createElement("br");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("input");
          dom.setAttribute(el3,"type","submit");
          dom.setAttribute(el3,"value","Add PHOTO to GALLERY");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, get = hooks.get, block = hooks.block, inline = hooks.inline, element = hooks.element;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var element1 = dom.childAt(fragment, [0, 0]);
          var element2 = dom.childAt(fragment, [1]);
          var element3 = dom.childAt(element2, [2, 1]);
          var morph0 = dom.createMorphAt(element1,1,1);
          var morph1 = dom.createMorphAt(dom.childAt(element1, [2]),0,0);
          var morph2 = dom.createMorphAt(element2,0,0);
          var morph3 = dom.createMorphAt(element2,1,1);
          block(env, morph0, context, "if", [get(env, context, "typedInsertion.image")], {}, child0, null);
          block(env, morph1, context, "em-file", [], {"property": "image", "cid": "file-field", "file": get(env, context, "file"), "dataURL": get(env, context, "dataURL")}, child1, null);
          inline(env, morph2, context, "em-input", [], {"property": "name", "label": "Name", "placeholder": "Add photo name...", "type": "text"});
          inline(env, morph3, context, "em-text", [], {"property": "description", "label": "DESCRIPTION", "placeholder": "Add photo description...", "rows": 3});
          element(env, element3, context, "bind-attr", [], {"class": ":button :success typedInsertion.isValid::disabled"});
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        block(env, morph0, context, "em-form", [], {"model": get(env, context, "typedInsertion"), "action": "createPhoto", "submit_button": false}, child0, null);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/templates/typed-insertion/add-transport-hub', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      return {
        isHTMLBars: true,
        revision: "Ember@1.11.0",
        blockParams: 0,
        cachedFragment: null,
        hasRendered: false,
        build: function build(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("label");
          var el2 = dom.createTextNode("Choose TRANSPORT HUB type");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1,"class","form-actions");
          var el2 = dom.createElement("br");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("input");
          dom.setAttribute(el2,"type","submit");
          dom.setAttribute(el2,"value","Add TRANSPORT HUB");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        render: function render(context, env, contextualElement) {
          var dom = env.dom;
          var hooks = env.hooks, get = hooks.get, inline = hooks.inline, element = hooks.element;
          dom.detectNamespace(contextualElement);
          var fragment;
          if (env.useFragmentCache && dom.canClone) {
            if (this.cachedFragment === null) {
              fragment = this.build(dom);
              if (this.hasRendered) {
                this.cachedFragment = fragment;
              } else {
                this.hasRendered = true;
              }
            }
            if (this.cachedFragment) {
              fragment = dom.cloneNode(this.cachedFragment, true);
            }
          } else {
            fragment = this.build(dom);
          }
          var element0 = dom.childAt(fragment, [4, 1]);
          var morph0 = dom.createMorphAt(fragment,1,1,contextualElement);
          var morph1 = dom.createMorphAt(fragment,2,2,contextualElement);
          var morph2 = dom.createMorphAt(fragment,3,3,contextualElement);
          inline(env, morph0, context, "simple-select", [], {"content": get(env, context, "transportHubTypes"), "action": "insertionSelectChanged", "prompt": "Please choose a hub type", "value": get(env, context, "typedInsertion.insertionType")});
          inline(env, morph1, context, "em-input", [], {"property": "name", "label": "Transport HUB NAME", "placeholder": "Add transport hub name...", "type": "text"});
          inline(env, morph2, context, "em-text", [], {"property": "description", "label": "Transport HUB DESCRIPTION", "placeholder": "Add transport hub description...", "rows": 3});
          element(env, element0, context, "bind-attr", [], {"class": ":button :success typedInsertion.isValid::disabled"});
          return fragment;
        }
      };
    }());
    return {
      isHTMLBars: true,
      revision: "Ember@1.11.0",
      blockParams: 0,
      cachedFragment: null,
      hasRendered: false,
      build: function build(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      render: function render(context, env, contextualElement) {
        var dom = env.dom;
        var hooks = env.hooks, get = hooks.get, block = hooks.block;
        dom.detectNamespace(contextualElement);
        var fragment;
        if (env.useFragmentCache && dom.canClone) {
          if (this.cachedFragment === null) {
            fragment = this.build(dom);
            if (this.hasRendered) {
              this.cachedFragment = fragment;
            } else {
              this.hasRendered = true;
            }
          }
          if (this.cachedFragment) {
            fragment = dom.cloneNode(this.cachedFragment, true);
          }
        } else {
          fragment = this.build(dom);
        }
        var morph0 = dom.createMorphAt(fragment,0,0,contextualElement);
        dom.insertBoundary(fragment, null);
        dom.insertBoundary(fragment, 0);
        block(env, morph0, context, "em-form", [], {"model": get(env, context, "typedInsertion"), "action": "createTransportHub", "submit_button": false}, child0, null);
        return fragment;
      }
    };
  }()));

});
define('outdoor-client/transforms/file', ['exports', 'ember-data'], function (exports, DS) {

  'use strict';

  exports['default'] = DS['default'].Transform.extend({
    deserialize: function(serialized) {
      return serialized;
    },

    serialize: function(deserialized) {
      return deserialized;
    }
  });

});
define('outdoor-client/utils/load-google-map', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  var loadGoogleMap;

  loadGoogleMap = function(resolveWith) {
    var $meta, promise, src;
    $meta = Ember['default'].$('meta[name="google-map-url"]');
    if ($meta.length) {
      src = $meta.attr('content');
      $meta.remove();
      return promise = new Ember['default'].RSVP.Promise(function(resolve, reject) {
        window.__emberGoogleMapLoaded__ = Ember['default'].run.bind(function() {
          promise = null;
          window.__emberGoogleMapLoaded__ = null;
          return resolve(resolveWith);
        });
        return Ember['default'].$.getScript(src + '&callback=__emberGoogleMapLoaded__').fail(function(jqXhr) {
          promise = null;
          window.__emberGoogleMapLoaded__ = null;
          return reject(jqXhr);
        });
      });
    } else if (promise) {
      return promise.then(function() {
        return resolveWith;
      });
    } else {
      return Ember['default'].RSVP.resolve(resolveWith);
    }
  };

  exports['default'] = loadGoogleMap;

});
define('outdoor-client/views/navbar', ['exports', 'ember', 'outdoor-client/mixins/foundation'], function (exports, Ember, FoundationMixin) {

  'use strict';

  var NavbarView;

  NavbarView = Ember['default'].View.extend(FoundationMixin['default'], {
    tagName: "nav",
    classNames: ["top-bar"],
    attributeBindings: ["isDataTopBar:data-topbar", "role"],
    isDataTopBar: "data-topbar",
    role: "navigation",
    templateName: 'navbar'
  });

  exports['default'] = NavbarView;

});
/* jshint ignore:start */

/* jshint ignore:end */

/* jshint ignore:start */

define('outdoor-client/config/environment', ['ember'], function(Ember) {
  var prefix = 'outdoor-client';
/* jshint ignore:start */

try {
  var metaName = prefix + '/config/environment';
  var rawConfig = Ember['default'].$('meta[name="' + metaName + '"]').attr('content');
  var config = JSON.parse(unescape(rawConfig));

  return { 'default': config };
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

/* jshint ignore:end */

});

if (runningTests) {
  require("outdoor-client/tests/test-helper");
} else {
  require("outdoor-client/app")["default"].create({"defaultLocale":"en"});
}

/* jshint ignore:end */
