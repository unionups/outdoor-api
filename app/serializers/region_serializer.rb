class RegionSerializer < ActiveModel::Serializer
  attributes :id, :name,  :description, :latitude, :longitude
  embed :ids
  has_one :country
  has_many :subregions
  has_many :transport_hubs
  has_many :contacts
  has_one :gallery
end
