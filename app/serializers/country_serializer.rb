class CountrySerializer < ActiveModel::Serializer
  attributes :id, :name, :cca2, :cca3, :latitude, :longitude, :description, :visa_info, :government_page, :transport_info
  embed :ids
  has_many :regions
  has_many :transport_hubs
  has_many :contacts
  has_one :gallery, embed: :object
end
