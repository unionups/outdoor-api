class SubregionSerializer < ActiveModel::Serializer
  attributes :id, :name,  :description, :latitude, :longitude
  embed :ids
  has_one :region
  has_many :transport_hubs
  has_many :contacts
  has_many :accommodations
  has_one :gallery
end
