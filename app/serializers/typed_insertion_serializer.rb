class TypedInsertionSerializer < ActiveModel::Serializer
  attributes :id, :insertion_type, :name, :description
end
