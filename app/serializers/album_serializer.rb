class AlbumSerializer < ActiveModel::Serializer
  attributes :id, :name, :description
  embed :ids
  has_one :gallery
  has_many :photos
end
