class GallerySerializer < ActiveModel::Serializer
  attributes :id, :location_type
  embed :ids
  has_one :location
  has_many :albums , embed: :objects
end
