class NamesSerializer < ActiveModel::Serializer
  attributes :id, :name, :latitude, :longitude
   def attributes
    data = super
    data[:cca2] = object.cca2 if object.try(:cca2)
    data[:cca3] = object.cca3 if object.try(:cca3)
    data
  end

end