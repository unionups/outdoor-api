class PhotoSerializer < ActiveModel::Serializer


  attributes :id, :name, :description , :image_random_thumb
  embed :ids
  has_many :albums

  private

    THUMB_SIZES = [ :small, :medium, :large ]

    def image_random_thumb
      object.image.url(THUMB_SIZES[rand(3)])
    end
end
