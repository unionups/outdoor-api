class SubregionsController < ApplicationController
  before_filter :authenticate_user_from_token!, except: [:index, :show, :subregion_names]
  before_filter :authenticate_user!, except: [:index, :show, :subregion_names]
  
  load_and_authorize_resource except: [:create, :subregion_names, :show ]

  respond_to :json

  # skip_before_filter :verify_authenticity_token
  # after_filter do
  #   p "sasdadasdasdsadadasdasdasdasdsadasdasdasd"
  #   p response.body
  # end
  


  def index    
    respond_with @subregions
  end



  def show     
    @subregion = Subregion.find(params[:id])
    authorize! :read, @subregion
    if @subregion.published == true 
      respond_with @subregion
    elsif !@subregion.present?
      render json: {}, status: 404
    else
      if authenticate_user_from_token! && (@subregion.publisher_id == current_user.id)
        respond_with @subregion
      else
        render json: @subregion, status: :unprocessable_entity
      end
    end
  end


  def create
    @subregion =  Subregion.new(subregion_params)
    @subregion.publisher_id = current_user.id
    authorize! :create, @subregion
    if @subregion.save()
      respond_with @subregion
    else 
      render json: {errors: @subregion.errors}, status: 422
    end
  end

  def subregion_names
    @subregion_names_published = Subregion.where("subregions.region_id = ? AND subregions.published ", params[:parent_location_id] )
    if authenticate_user_from_token!
      @subregion_names_publisher = Subregion.where("subregions.region_id = ? AND subregions.publisher_id = ? ", params[:parent_location_id] , current_user.id) 
      if @subregion_names_publisher
        @subregion_names = @subregion_names_publisher + @subregion_names_published if @subregion_names_publisher
        @subregion_names.compact!
        @subregion_names.uniq!
      end
    end
    respond_with @subregion_names, each_serializer: NamesSerializer,  root: "subregion_names"
  end

  private

    def subregion_params
      params.require(:subregion).permit(:name,  :description, :region_id, :latitude, :longitude, 
        transport_hubs_attributes: [ :insertion_type, :name, :description ] , 
        contacts_attributes: [ :insertion_type, :name, :description ],
        accommodations_attributes: [ :insertion_type, :name, :description ]) if params[:subregion]
    end
end
