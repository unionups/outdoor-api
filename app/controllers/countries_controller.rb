class CountriesController < ApplicationController


  before_filter :authenticate_user_from_token!, except: [:index, :show, :country_names, :countries_list]
  before_filter :authenticate_user!, except: [:index, :show, :country_names, :countries_list]
  
  load_and_authorize_resource except: [:create, :country_names, :countries_list ,:show ]

  respond_to :json

  # skip_before_filter :verify_authenticity_token
  # after_filter do
  #   p "sasdadasdasdsadadasdasdasdasdsadasdasdasd"
  #   p response.body
  # end
  


  def index    
    respond_with @countries
  end

  def update 
    @country = Country.find(params[:id])
    if current_user.role?(:moderator)
      @country = Country.update_attributes(country_params)
      @country.publish()
      if @country.save()
        respond_with @country
      else 
        render json: {errors: @country.errors}, status: 422
      end
    elsif current_user.role?(:user)
      @country_clone =  Country.new(country_params)
      @country_clone.clone_of_id @country.id
      @country_clone.mark_as_changed(@country)
      if @country_clone.save() && @country.save()
        respond_with @country_clone
      else 
        render json: {errors: @country_clone.errors}, status: 422
      end
    else
      render json: {}, status: 403
    end
    
  end


  def show     
    @country = Country.find(params[:id])
    authorize! :read, @country
    if @country.published?
      respond_with @country
    elsif !@country.present?
      render json: {}, status: 404
    else
      if authenticate_user_from_token! && (@country.publisher_id == current_user.id)
        respond_with @country 
      else
        render json: @country, status: :unprocessable_entity
      end
    end

  end

  def create
    @country =  Country.new(country_params)
    @country.publisher_id = current_user.id
    authorize! :create, @country
    if @country.save()
      respond_with @country
    else 
      render json: {errors: @country.errors}, status: 422
    end
  end

  def country_names
    @country_names = Country.published
    @country_names += Country.where(publisher_id: current_user.id) if authenticate_user_from_token!
    respond_with @country_names, each_serializer: NamesSerializer,  root: "country_names"
  end

  def countries_list
    @country_names = Country.published
    @country_names += Country.where(publisher_id: current_user.id) if authenticate_user_from_token!
    @country_names = @country_names.as_json(only: [:name, :cca2, :cca3, :latitude, :longitude])
    @country_names = @country_names.map{ |cn| cn.symbolize_keys }
    @country_names.compact!
    @country_names.uniq!
    
    @countries_list = COUNTRIES_LIST
    @countries_list = @countries_list.map do |country| 
      latlng = country[:latlng]
      country = country.slice :name, :cca2, :cca3 
      country[:name] = country[:name][:common]
      country[:latitude]=latlng[0]
      country[:longitude]=latlng[1]
      country
    end

    @countries_list = @countries_list - @country_names
    render json: @countries_list.to_json

  end

  private

    def country_params
      params.require(:country).permit(:name, :cca2, :cca3, :latitude, :longitude, :description, :visa_info,
        :government_page, :transport_info, gallery_attributes: [ albums_attributes: [:name, :description, photos_attributes: [:name, :description, :image]] ],  transport_hubs_attributes: [ :insertion_type, :name, :description ] , contacts_attributes: [ :insertion_type, :name, :description ]) if params[:country]
    end


end
