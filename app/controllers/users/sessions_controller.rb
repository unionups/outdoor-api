class Users::SessionsController < Devise::SessionsController
  before_filter :configure_sign_in_params, only: [:create]


  # GET /resource/sign_in
  # def new
  #   super
  # end

  # POST /resource/sign_in
  # def create
  #   super
  # end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # You can put the params you want to permit in the empty array.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.for(:sign_in) << :attribute
  # end




  def create
    self.resource = warden.authenticate!(auth_options)
    sign_in(resource_name, resource)
    data = {
      token: self.resource.authentication_token,
      email: self.resource.email,
      role: self.resource.role,
      lang: self.resource.lang
    }
    render json: data, status: 201
  end

  def configure_sign_in_params
    devise_parameter_sanitizer.for(:sign_in) << :email
  end
  
end
