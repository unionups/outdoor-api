class RegionsController < ApplicationController
  before_filter :authenticate_user_from_token!, except: [:index, :show, :region_names]
  before_filter :authenticate_user!, except: [:index, :show, :region_names]
  
  load_and_authorize_resource except: [:create, :region_names, :show ]

  respond_to :json

  # skip_before_filter :verify_authenticity_token
  after_filter do
    p "sasdadasdasdsadadasdasdasdasdsadasdasdasd"
    p response.body
  end
  


  def index    
    respond_with @regions
  end



  def show     
    @region = Region.find(params[:id])
    authorize! :read, @region
    if @region.published == true 
      respond_with @region
    elsif !@region.present?
      render json: {}, status: 404
    else
      if authenticate_user_from_token! && (@region.publisher_id == current_user.id)
        respond_with @region
      else
        render json: @region, status: :unprocessable_entity
      end
    end

  end

  def create
    @region =  Region.new(region_params)
    @region.publisher_id = current_user.id
    authorize! :create, @region
    if @region.save()
      respond_with @region
    else 
      render json: {errors: @region.errors}, status: 422
    end
  end

  def region_names
    @region_names_published = Region.where("regions.country_id = ? AND regions.published ", params[:parent_location_id] )
    if authenticate_user_from_token!
      @region_names_publisher = Region.where("regions.country_id = ? AND regions.publisher_id = ? ", params[:parent_location_id] , current_user.id) 
      if @region_names_publisher
        @region_names = @region_names_publisher + @region_names_published if @region_names_publisher
        @region_names.compact!
        @region_names.uniq!
      end
    end
    respond_with @region_names, each_serializer: NamesSerializer,  root: "region_names"
  end

  private

    def region_params
      params.require(:region).permit(:name,  :description, :latitude, :longitude, :country_id, transport_hubs_attributes: [ :insertion_type, :name, :description ] , contacts_attributes: [ :insertion_type, :name, :description ]) if params[:region]
    end
end
