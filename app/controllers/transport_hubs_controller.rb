class TransportHubsController < ApplicationController
  include CanCan::ControllerAdditions
  before_action :authenticate_user!, except: [:index, :show]

  respond_to :json


  def index    
    @transport_hubs = TransportHub.all
    authorize! :read, @transport_hubs
    respond_with @transport_hubs, each_serializer: TypedInsertionSerializer , root: "transport_hubs"
  end



  def show     
    @transport_hub = TransportHub.find(params[:id])
    authorize! :read, @transport_hub
    respond_with @transport_hub,  serializer: TypedInsertionSerializer , root: "transport_hub"

  end
end
