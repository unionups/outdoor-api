class ContactsController < ApplicationController
  include CanCan::ControllerAdditions
  before_action :authenticate_user!, except: [:index, :show]

  respond_to :json

  def index    
    @contacts = Contact.all
    authorize! :read, @contacts
    respond_with @contacts, each_serializer: TypedInsertionSerializer , root: "contacts"

  end



  def show     
    @contact = Contact.find(params[:id])
    authorize! :read, @contact
    respond_with @contact,  serializer: TypedInsertionSerializer , root: "contact"

  end
end
