class ApplicationController < ActionController::API
  include ActionController::MimeResponds
  include ActionController::ImplicitRender
  include ActionController::HttpAuthentication::Token::ControllerMethods
  # include ActionController::RequestForgeryProtection
  include CanCan::ControllerAdditions

  # before_filter do
  #   p request
  # end

  # protect_from_forgery with: :null_session


  respond_to :json

  skip_before_filter :verify_authenticity_token
  


 

  rescue_from CanCan::AccessDenied do |exception|
    render json: {}, status: 403
  end



  private

    def authenticate_user_from_token!
      authenticate_with_http_token do |token, options|
        user_email = options[:email].presence
        user       = user_email && User.find_by_email(user_email)
        if user && Devise.secure_compare(user.authentication_token, token)
          sign_in user, store: false
        end
      end
    end

    
end
