class AccommodationsController < ApplicationController
  include CanCan::ControllerAdditions
  before_action :authenticate_user!, except: [:index, :show]

  respond_to :json


  def index    
    @accommodations = Accommomdation.all
    authorize! :read, @accommodations
    respond_with @accommodations, each_serializer: TypedInsertionSerializer , root: "accommodations"
  end



  def show     
    @accommodation = Accommodation.find(params[:id])
    authorize! :read, @accommodation
    respond_with @accommodation,  serializer: TypedInsertionSerializer , root: "accommodation"

  end
end
