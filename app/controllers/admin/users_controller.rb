class Admin::UsersController < ApplicationController
  before_action :authenticate_user_from_token!
  # load_and_authorize_resources # issue in production!!!!!!!!!!!!!!
  # load_and_authorize_resource :user
 
  load_and_authorize_resource

  respond_to :json

  def index    
    respond_with @users  
  end

  def show     
    respond_with @user     
  end

end
