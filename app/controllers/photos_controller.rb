class PhotosController < ApplicationController

  before_filter :authenticate_user_from_token!, except: [:index, :show]
  before_filter :authenticate_user!, except: [:index, :show]
  
  load_and_authorize_resource except: [:create ]

  respond_to :json

  def create
    params[:photo][:album_ids] = params[:photo][:album_ids][1..-1]
    @photo =  Photo.new(photo_params)
    @photo.publisher_id = current_user.id
    authorize! :create, @photo
    if @photo.save()
      respond_with @photo
    else 
      render json: {errors: @photo.errors}, status: 422
    end
  end

  private

    def photo_params
      params.require(:photo).permit(:image, :name, :description, :album_ids)
    end

end
