class Album < ActiveRecord::Base
  include ModerableProtect
  include ModeratorUtils
  
  belongs_to :gallery
  has_and_belongs_to_many :photos #, join_table: :albums_photos

  accepts_nested_attributes_for :photos, allow_destroy: true
end
