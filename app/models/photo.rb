class Photo < ActiveRecord::Base
  include ModerableProtect

  
  has_and_belongs_to_many :albums 
  has_attached_file :image, styles: {
                      small:  '200x200>',
                      medium: '400X400>',
                      large: '600X400>'
                    }
  validates_attachment :image, 
    presence: true, 
    content_type: { content_type: ["image/jpg", "image/jpeg", "image/png"] }
end
