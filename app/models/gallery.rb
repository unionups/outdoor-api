class Gallery < ActiveRecord::Base
  include ModerableProtect
  include ModeratorUtils
  
  has_many :albums
  belongs_to :location, polymorphic: true 

  accepts_nested_attributes_for :albums, allow_destroy: true
end
