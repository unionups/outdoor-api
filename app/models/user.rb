class User < ActiveRecord::Base
  before_save :ensure_authentication_token

  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable



  ROLES = %w[ user moderator admin ]

  def role?(base_role)
    ROLES.index(base_role.to_s) <= ROLES.index(role)
  end


  protect do |user|
    if !user.nil?
      if user.role? :admin
        scope { all } 
        can :read                             
        can :create                           
        can :update                           
        can :destroy, id: labmda { |id|      
          id != user.id                        
        } 

      elseif  user.role? :moderator
        scope { where(role: "user") }
        can :read
      end

    end
  end

  def ensure_authentication_token
    if authentication_token.blank?
      self.authentication_token = generate_authentication_token
    end
  end

  private

    def generate_authentication_token
      loop do
        token = Devise.friendly_token
        break token unless User.where(authentication_token: token).first
      end
    end
end
