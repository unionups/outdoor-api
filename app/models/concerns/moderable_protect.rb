module ModerableProtect
  extend ActiveSupport::Concern
    
  included do 
    protect do |user, resource|
      if !user.nil? 
        scope { all } 
        can :read
        if resource.try(:publisher_id) == user.id   
          can :create                           
          can :update   
          can :destroy      
        end
        if user.role? :moderator
          can :create                           
          can :update 
          can :publish
          can :unpublish
          can :backup
          if user.role? :admin
            can :destroy  
          end
        end 
      else
        scope { where(published: true) } 
        can :read
      end
    end
  end

end