module ModeratorUtils
  extend ActiveSupport::Concern

  def publish
    self.update_attribute :published, true
  end

  def unpublish
    self.update_attribute :published, false
  end

  def clone_of_id(id)
    self.update_attribute :clone_of_id, id
  end

  def mark_as_changed(original)
    @changed_items = []
    original.columns_hash.each do |key, value|
      if self[key] == value
        @changed_items << key.to_s()
      end  
    end
    @original_changed_items = original.changed_items.scan /\w/

    @original_changed_items << @changed_items
    @original_changed_items.compact!
    @original_changed_items.uniq!

    original.update_attribute :changed_items, @original_changed_items.join(" ")
    original.update_attribute :is_changed, true
    self.update_attribute :changed_items, @changed_items.join(" ")
    self.update_attribute :is_changed, true
  end

  def unchanged
    self.update_attributes is_changed: false, changed_items: ""
  end

  def to_backup
    self.update_attribute :is_backup, true
  end

  def published?
    return self.published
  end

  def backup?
    return self.is_backup
  end
end