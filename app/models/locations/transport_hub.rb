class TransportHub < TypedInsertion 
  include ModeratorUtils
  
  TRANSPORT_HUB_TYPES = %w[ airport seaport railway bus taxi hitchhiking public other ]

  validates :insertion_type, inclusion: { in: TRANSPORT_HUB_TYPES }

end
