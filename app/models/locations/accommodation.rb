class Accommodation < TypedInsertion
  include ModeratorUtils

  ACCOMMODATION_TYPES = %w[ hotel guesthouse wildcamping other ]

  validates :insertion_type, inclusion: { in: ACCOMMODATION_TYPES }
  
end  