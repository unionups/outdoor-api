class Region < ActiveRecord::Base
  include ModerableProtect
  include ModeratorUtils
  
  belongs_to :country

  has_many :subregions, dependent: :destroy
  has_many :transport_hubs, as: :location, dependent: :destroy
  has_many :contacts, as: :location, dependent: :destroy
  has_one  :gallery, as: :location, dependent: :destroy
  
  accepts_nested_attributes_for :transport_hubs, :contacts, allow_destroy: true
  
  scope :published, -> { where(published: true) }

  

end
